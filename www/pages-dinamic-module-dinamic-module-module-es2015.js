(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dinamic-module-dinamic-module-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dinamic-module/dinamic-module.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dinamic-module/dinamic-module.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar  style=\"text-align: right;\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"8\">\n        <div>\n          <ion-title></ion-title>\n        </div>\n      </ion-col>\n      <ion-col size=\"4\">\n        <div>\n          <ion-img src=\"assets/ifrem.png\" style=\"margin-right: 5%; height: 35px;\"></ion-img>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-item>\n    <ion-label>Fecha Inicial</ion-label>\n    <!-- <ion-datetime value=\"2019-10-01T15:43:40.394Z\" display-timezone=\"utc\" displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"fechaInicial\"></ion-datetime> -->\n    <ion-datetime min=\"1000-01-01\" max=\"2050-12-01\" displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"fechaInicial\"></ion-datetime>\n  </ion-item>\n  <ion-item>\n    <ion-label>Fecha Final</ion-label>\n    <ion-datetime min=\"1000-01-01\" max=\"2050-12-01\" displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"fechaFinal\"></ion-datetime>\n  </ion-item>\n   <ion-item>\n    <ion-label>Tipo de Gráfica</ion-label>\n    <ion-select [(ngModel)]=\"tipoGrafica\">\n      <ion-select-option value=\"line\">Lineal</ion-select-option>\n      <ion-select-option value=\"bar\">Barra verticales</ion-select-option>\n      <ion-select-option value=\"horizontalBar\">Barras Horizontales</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <!-- <ion-item>\n    <ion-label>Tipo de Gráfica</ion-label>\n    <ion-select [(ngModel)]=\"tipoGrafica\">\n      <ion-select-option value=\"line\">Lineal</ion-select-option>\n      <ion-select-option value=\"bar\">Barra verticales</ion-select-option>\n      <ion-select-option value=\"horizontalBar\">Barras Horizontales</ion-select-option>\n      <ion-select-option value=\"radar\">Radar</ion-select-option>\n      <ion-select-option value=\"pie\">Pastel</ion-select-option>\n      <ion-select-option value=\"polarArea\">Polar</ion-select-option>\n      <ion-select-option value=\"bubble\">burbuja</ion-select-option>\n    </ion-select>\n  </ion-item> -->\n\n    <ion-button type=\"submit\" expand=\"full\" (click)=\"generate()\">Generar Gráfica</ion-button>\n  <div style=\"display: block\">\n    <canvas id=\"chartGraf\" width=\"500\" height=\"400\"></canvas>\n  </div>\n\n  <ion-item *ngIf=\"totFre\">\n    <ion-label color=\"primary\">Total de FRE: <strong> {{totFre}} </strong></ion-label>\n    <!-- <ion-button slot=\"end\" (click)=\"descargarGrafica()\">Descargar</ion-button> -->\n  </ion-item>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/dinamic-module/dinamic-module-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/dinamic-module/dinamic-module-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: DinamicModulePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DinamicModulePageRoutingModule", function() { return DinamicModulePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _dinamic_module_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dinamic-module.page */ "./src/app/pages/dinamic-module/dinamic-module.page.ts");




const routes = [
    {
        path: '',
        component: _dinamic_module_page__WEBPACK_IMPORTED_MODULE_3__["DinamicModulePage"]
    }
];
let DinamicModulePageRoutingModule = class DinamicModulePageRoutingModule {
};
DinamicModulePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DinamicModulePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/dinamic-module/dinamic-module.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/dinamic-module/dinamic-module.module.ts ***!
  \***************************************************************/
/*! exports provided: DinamicModulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DinamicModulePageModule", function() { return DinamicModulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _dinamic_module_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dinamic-module-routing.module */ "./src/app/pages/dinamic-module/dinamic-module-routing.module.ts");
/* harmony import */ var _dinamic_module_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dinamic-module.page */ "./src/app/pages/dinamic-module/dinamic-module.page.ts");







let DinamicModulePageModule = class DinamicModulePageModule {
};
DinamicModulePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _dinamic_module_routing_module__WEBPACK_IMPORTED_MODULE_5__["DinamicModulePageRoutingModule"]
        ],
        declarations: [_dinamic_module_page__WEBPACK_IMPORTED_MODULE_6__["DinamicModulePage"]]
    })
], DinamicModulePageModule);



/***/ }),

/***/ "./src/app/pages/dinamic-module/dinamic-module.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/dinamic-module/dinamic-module.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".centrado {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbGFyYS9hcHBzXzIwMjAvTEFCL3YyL3RhYmxlcm8taWZyZW0vc3JjL2FwcC9wYWdlcy9kaW5hbWljLW1vZHVsZS9kaW5hbWljLW1vZHVsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2RpbmFtaWMtbW9kdWxlL2RpbmFtaWMtbW9kdWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kaW5hbWljLW1vZHVsZS9kaW5hbWljLW1vZHVsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2VudHJhZG97XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9IiwiLmNlbnRyYWRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/dinamic-module/dinamic-module.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/dinamic-module/dinamic-module.page.ts ***!
  \*************************************************************/
/*! exports provided: DinamicModulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DinamicModulePage", function() { return DinamicModulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/alert.service */ "./src/app/services/alert.service.ts");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/dist/Chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");






let DinamicModulePage = class DinamicModulePage {
    constructor(authService, alertService, datePipe) {
        this.authService = authService;
        this.alertService = alertService;
        this.datePipe = datePipe;
        this.fechaInicial = '';
        this.fechaFinal = '';
        this.totAceptados = 0;
        this.totAutomaticos = 0;
        this.totClg = 0;
        this.totFirmados = 0;
        this.totIngresados = 0;
        this.totOrdinarios = 0;
        this.totPortal = 0;
        this.totRecaudado = 0;
        this.totRechazados = 0;
        this.totSuspendidos = 0;
        this.tipoGrafica = '';
        this.listLabels = [
            'Aceptados',
            'Automáticos',
            'CLGs',
            'Firmados',
            'Ingresados',
            'Ordinarios',
            'Portal',
            'Recaudado',
            'Rechazados',
            'Suspendidos'
        ];
        this.listTotales = [];
        this.listColors = [
            '#ffc409'
        ];
    }
    ngOnInit() {
        // this.showChart();
    }
    generate() {
        console.log('fecha inicial: ' + this.fechaInicial.toString());
        console.log(this.datePipe.transform(this.fechaInicial, 'dd/MM/yyyy'));
        console.log('Fecha final: ' + this.fechaFinal.toString());
        if (this.fechaFinal !== '' && this.fechaInicial !== '' && this.tipoGrafica !== '') {
            // if (this.fechaFinal !== '' && this.fechaInicial !== '') {
            this.showChart();
        }
        else {
            this.alertService.presentToast('FALTAN CAMPOS POR SELECIONAR');
        }
    }
    showChart() {
        let i = -1;
        const colors = this.listLabels.map(_ => this.listColors[(i = (i + 1) % this.listColors.length)]);
        if (this.chart != null) {
            this.chart.destroy();
        }
        this.ctx = document.getElementById('chartGraf').getContext('2d');
        this.chart = new chart_js__WEBPACK_IMPORTED_MODULE_4__["Chart"](this.ctx, {
            type: this.tipoGrafica,
            data: {
                labels: this.listLabels,
                datasets: []
            },
            options: {
                responsive: true,
                scales: {
                    xAxes: [{
                            stacked: true
                        }],
                    yAxes: [{
                            stacked: true
                        }]
                }
            }
        });
        // this.addData(this.chart, 'ENERO', '#ffc409', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
        // this.addData(this.chart, 'Febero', '#ef4c29', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
        this.generateData();
        this.chart.update();
        // this.resetValues();
        console.log('COLOR: ' + this.genertaColor());
    }
    generateData() {
        // tslint:disable-next-line:max-line-length
        this.authService.getFechas(this.datePipe.transform(this.fechaInicial, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy')).then(data => {
            this.datosFecha = data;
            console.log('TOTAL FECHAS: ' + this.datosFecha[0].totalFres + ' Total:' + this.datosFecha.length);
            console.log(data);
            let monthName = '';
            let monthNameAux = '';
            let contAux = 0;
            this.datosFecha.forEach(element => {
                contAux++;
                console.log('Contdor: ' + contAux);
                const args = element.fechaCorte.split(' ');
                const argsN = args[0].split('/');
                const fechaNueva = argsN[1] + '/' + argsN[0] + '/' + argsN[2];
                let date = new Date(fechaNueva).toDateString();
                console.log('Fecha in: ' + date);
                date = this.datePipe.transform(date, 'MM/dd/yyyy');
                console.log('fecha nueva: ' + date);
                console.log('----------datos-------');
                console.log(this.datePipe.transform(date, 'MMMM'));
                console.log(this.datePipe.transform(date, 'yyyy'));
                console.log(element.totalAceptados);
                monthName = this.datePipe.transform(date, 'MMMM');
                if ((monthName === monthNameAux) || (monthNameAux === '')) {
                    monthNameAux = this.datePipe.transform(date, 'MMMM');
                    this.totAceptados = this.totAceptados + element.totalAceptados;
                    this.totAutomaticos = this.totAutomaticos + element.totalAuto;
                    this.totClg = this.totClg + element.totalCLG;
                    this.totFirmados = this.totFirmados + element.totalFirmados;
                    // this.totFre = 0;
                    this.totIngresados = this.totIngresados + element.totalIngresados;
                    this.totOrdinarios = this.totOrdinarios + element.totalOrdinario;
                    this.totPortal = this.totPortal + element.totalPortal;
                    this.totRecaudado = this.totRecaudado + element.totalRecaudo;
                    this.totRechazados = this.totRechazados + element.totalRechazo;
                    this.totSuspendidos = this.totSuspendidos + element.totalSuspendido;
                    if (this.datosFecha.length === contAux) {
                        this.llenarListaTotales();
                        const randColor = this.genertaColor();
                        this.addData(this.chart, monthNameAux + '/' + this.datePipe.transform(date, 'yyyy'), randColor, this.listTotales);
                        this.resetValues();
                    }
                }
                else {
                    this.llenarListaTotales();
                    const randColor = this.genertaColor();
                    this.addData(this.chart, monthNameAux + '/' + this.datePipe.transform(date, 'yyyy'), randColor, this.listTotales);
                    monthNameAux = this.datePipe.transform(date, 'MMMM');
                    this.resetValues();
                    this.totAceptados = this.totAceptados + element.totalAceptados;
                    this.totAutomaticos = this.totAutomaticos + element.totalAuto;
                    this.totClg = this.totClg + element.totalCLG;
                    this.totFirmados = this.totFirmados + element.totalFirmados;
                    // this.totFre = 0;
                    this.totIngresados = this.totIngresados + element.totalIngresados;
                    this.totOrdinarios = this.totOrdinarios + element.totalOrdinario;
                    this.totPortal = this.totPortal + element.totalPortal;
                    this.totRecaudado = this.totRecaudado + element.totalRecaudo;
                    this.totRechazados = this.totRechazados + element.totalRechazo;
                    this.totSuspendidos = this.totSuspendidos + element.totalSuspendido;
                }
                this.totFre = element.totalFres;
            });
            // console.log('Aceptados: ' + this.totAceptados);
            // console.log('Automaticos: ' + this.totAutomaticos);
        });
    }
    llenarListaTotales() {
        this.listTotales.push(this.totAceptados);
        this.listTotales.push(this.totAutomaticos);
        this.listTotales.push(this.totClg);
        this.listTotales.push(this.totFirmados);
        // this.listTotales.push(this.totFre);
        this.listTotales.push(this.totIngresados);
        this.listTotales.push(this.totOrdinarios);
        this.listTotales.push(this.totPortal);
        this.listTotales.push(this.totRecaudado);
        this.listTotales.push(this.totRechazados);
        this.listTotales.push(this.totSuspendidos);
    }
    addData(chart, lab, color, dat) {
        chart.data.datasets.push({
            label: lab,
            backgroundColor: color,
            borderColor: color,
            data: dat,
            fill: false,
            pointStyle: 'rectRot',
            pointRadius: 5,
            pointBorderColor: 'rgb(0, 0, 0)'
        });
        chart.update();
    }
    genertaColor() {
        let simbolos, color;
        simbolos = '0123456789ABCDEF';
        color = '#';
        for (let i = 0; i < 6; i++) {
            color = color + simbolos[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    descargarGrafica() {
    }
    resetValues() {
        this.totAceptados = 0;
        this.totAutomaticos = 0;
        this.totClg = 0;
        this.totFirmados = 0;
        this.totFre = 0;
        this.totIngresados = 0;
        this.totOrdinarios = 0;
        this.totPortal = 0;
        this.totRecaudado = 0;
        this.totRechazados = 0;
        this.totSuspendidos = 0;
        this.listTotales = [];
    }
};
DinamicModulePage.ctorParameters = () => [
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"] }
];
DinamicModulePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dinamic-module',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dinamic-module.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dinamic-module/dinamic-module.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dinamic-module.page.scss */ "./src/app/pages/dinamic-module/dinamic-module.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
        src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]])
], DinamicModulePage);



/***/ })

}]);
//# sourceMappingURL=pages-dinamic-module-dinamic-module-module-es2015.js.map