function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-finanzas-finanzas-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/finanzas/finanzas.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/finanzas/finanzas.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesFinanzasFinanzasPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar  style=\"text-align: right;\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"8\">\n        <div>\n          <ion-title></ion-title>\n        </div>\n      </ion-col>\n      <ion-col size=\"4\">\n        <div>\n          <ion-img src=\"assets/ifrem.png\" style=\"margin-right: 5%; height: 35px;\"></ion-img>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n\n  <style>\n  .red{\n      color: red;\n    }\n    .green{\n      color: green;\n    }\n  </style>\n</ion-header>\n\n<ion-content>\n    \n   \n\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"11\">\n                <canvas #lineCanvas width=\"100%\" height=\"100%\"></canvas>\n\n            </ion-col>\n          <ion-col size=\"1\"></ion-col>\n\n          </ion-row>\n    </ion-grid>\n    \n  <div *ngIf=\"recaudacion\">  \n  <ion-list>\n        <!-- <ion-list-header>List Notes</ion-list-header> -->\n             <ion-item  style=\"font-size: 0.6em;\">\n                <ion-col size=\"3\">\n                    PERIODO\n                </ion-col>\n                <ion-col size=\"3\">\n                    PROYECCIÓN\n                  </ion-col>\n                  <ion-col size=\"3\">\n                    RECAUDACIÓN REAL\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        RESULTADO\n                    </ion-col>\n             </ion-item>\n             <ion-item  *ngFor=\"let ingreso of recaudacion\" style=\"font-size: 0.5em;\">\n                <ion-col size=\"3\">\n                    {{ ingreso.periodo | date: \"MMM/yyyy\" }}\n                </ion-col>\n                <ion-col size=\"3\">\n                    {{ ingreso.proyeccion | currency:'MXN':true:'1.0-0' }}\n                  </ion-col>\n                  <ion-col size=\"3\">\n                      {{ ingreso.recaudado | currency:'MXN':true:'1.0-0' }}\n                    </ion-col>\n                    <ion-col size=\"3\">\n        \n                        <span  class=\"{{ ingreso.recaudado - ingreso.proyeccion <0 ? 'red' : 'green' }}\">\n                            {{ ingreso.recaudado - ingreso.proyeccion | currency:'MXN':true:'1.0-0' }} <ion-icon name=\"{{ ingreso.recaudado - ingreso.proyeccion <0 ? 'arrow-down-outline' : 'arrow-up-outline' }}\"></ion-icon>\n                          </span>\n                        \n                      </ion-col>\n\n              </ion-item>\n    </ion-list>  \n  </div>  \n\n\n  <div *ngIf=\"recaudacionCantidad\"> \n    \n      <ion-header translucent>\n          <ion-toolbar>\n            <ion-title>Top de Trámites por Demanda</ion-title>\n          </ion-toolbar>\n        </ion-header>\n\n      <ion-list>\n            <!-- <ion-list-header>List Notes</ion-list-header> -->\n                 <ion-item  style=\"font-size: 0.6em;\">\n                    <ion-col size=\"3\">\n                        PERIODO\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        PROYECCIÓN\n                      </ion-col>\n                      <ion-col size=\"3\">\n                        RECAUDACIÓN REAL\n                        </ion-col>\n                        <ion-col size=\"3\">\n                            RESULTADO\n                        </ion-col>\n                 </ion-item>\n                 <ion-item  *ngFor=\"let ingreso of recaudacionCantidad\" style=\"font-size: 0.5em;\">\n                    <ion-col size=\"3\">\n                        {{ ingreso.nombreActo }}\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        {{ ingreso.nombreServicio }}\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        {{ ingreso.cantidad | number}}\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        {{ ingreso.recaudado | currency:'MXN':true:'1.0-0' }}\n                    </ion-col>\n    \n                  </ion-item>\n        </ion-list> \n  </div>\n\n \n  <div *ngIf=\"recaudacionDinero\"> \n  <ion-header translucent>\n      <ion-toolbar>\n        <ion-title>Top de Trámites por Recaudación</ion-title>\n      </ion-toolbar>\n    </ion-header>\n    <ion-list>\n        <!-- <ion-list-header>List Notes</ion-list-header> -->\n             <ion-item  style=\"font-size: 0.6em;\">\n                <ion-col size=\"3\">\n                    PERIODO\n                </ion-col>\n                <ion-col size=\"3\">\n                    PROYECCIÓN\n                  </ion-col>\n                  <ion-col size=\"3\">\n                    RECAUDACIÓN REAL\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        RESULTADO\n                    </ion-col>\n             </ion-item>\n             <ion-item  *ngFor=\"let ingreso of recaudacionDinero\" style=\"font-size: 0.5em;\">\n                <ion-col size=\"3\">\n                    {{ ingreso.nombreActo }}\n                </ion-col>\n                <ion-col size=\"3\">\n                    {{ ingreso.nombreServicio }}\n                </ion-col>\n                <ion-col size=\"3\">\n                    {{ ingreso.cantidad | number}}\n                </ion-col>\n                <ion-col size=\"3\">\n                    {{ ingreso.recaudado | currency:'MXN':true:'1.0-0' }}\n                </ion-col>\n\n              </ion-item>\n    </ion-list> \n</div>\n\n<div *ngIf=\"recaudacionDineroIns\"> \n    <ion-header translucent>\n        <ion-toolbar>\n          <ion-title>Top de Trámites por Recaudación Inscripciones</ion-title>\n        </ion-toolbar>\n      </ion-header>\n      <ion-list>\n          <!-- <ion-list-header>List Notes</ion-list-header> -->\n               <ion-item  style=\"font-size: 0.6em;\">\n                  <ion-col size=\"3\">\n                      PERIODO\n                  </ion-col>\n                  <ion-col size=\"3\">\n                      PROYECCIÓN\n                    </ion-col>\n                    <ion-col size=\"3\">\n                      RECAUDACIÓN REAL\n                      </ion-col>\n                      <ion-col size=\"3\">\n                          RESULTADO\n                      </ion-col>\n               </ion-item>\n               <ion-item  *ngFor=\"let ingreso of recaudacionDineroIns\" style=\"font-size: 0.5em;\">\n                  <ion-col size=\"3\">\n                      {{ ingreso.nombreActo }}\n                  </ion-col>\n                  <ion-col size=\"3\">\n                      {{ ingreso.nombreServicio }}\n                  </ion-col>\n                  <ion-col size=\"3\">\n                      {{ ingreso.cantidad | number}}\n                  </ion-col>\n                  <ion-col size=\"3\">\n                      {{ ingreso.recaudado | currency:'MXN':true:'1.0-0' }}\n                  </ion-col>\n  \n                </ion-item>\n      </ion-list> \n  </div>\n\n  <div *ngIf=\"recaudacionDineroCert\"> \n      <ion-header translucent>\n          <ion-toolbar>\n            <ion-title>Top de Trámites por Recaudación Certificaciones</ion-title>\n          </ion-toolbar>\n        </ion-header>\n        <ion-list>\n            <!-- <ion-list-header>List Notes</ion-list-header> -->\n                 <ion-item  style=\"font-size: 0.6em;\">\n                    <ion-col size=\"3\">\n                        PERIODO\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        PROYECCIÓN\n                      </ion-col>\n                      <ion-col size=\"3\">\n                        RECAUDACIÓN REAL\n                        </ion-col>\n                        <ion-col size=\"3\">\n                            RESULTADO\n                        </ion-col>\n                 </ion-item>\n                 <ion-item  *ngFor=\"let ingreso of recaudacionDineroCert\" style=\"font-size: 0.5em;\">\n                    <ion-col size=\"3\">\n                        {{ ingreso.nombreActo }}\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        {{ ingreso.nombreServicio }}\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        {{ ingreso.cantidad | number}}\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        {{ ingreso.recaudado | currency:'MXN':true:'1.0-0' }}\n                    </ion-col>\n    \n                  </ion-item>\n        </ion-list> \n    </div>\n  \n</ion-content>\n\n";
    /***/
  },

  /***/
  "./src/app/pages/finanzas/finanzas-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/finanzas/finanzas-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: FinanzasPageRoutingModule */

  /***/
  function srcAppPagesFinanzasFinanzasRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FinanzasPageRoutingModule", function () {
      return FinanzasPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _finanzas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./finanzas.page */
    "./src/app/pages/finanzas/finanzas.page.ts");

    var routes = [{
      path: '',
      component: _finanzas_page__WEBPACK_IMPORTED_MODULE_3__["FinanzasPage"]
    }];

    var FinanzasPageRoutingModule = function FinanzasPageRoutingModule() {
      _classCallCheck(this, FinanzasPageRoutingModule);
    };

    FinanzasPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], FinanzasPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/finanzas/finanzas.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/finanzas/finanzas.module.ts ***!
    \***************************************************/

  /*! exports provided: FinanzasPageModule */

  /***/
  function srcAppPagesFinanzasFinanzasModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FinanzasPageModule", function () {
      return FinanzasPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _finanzas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./finanzas-routing.module */
    "./src/app/pages/finanzas/finanzas-routing.module.ts");
    /* harmony import */


    var _finanzas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./finanzas.page */
    "./src/app/pages/finanzas/finanzas.page.ts");

    var FinanzasPageModule = function FinanzasPageModule() {
      _classCallCheck(this, FinanzasPageModule);
    };

    FinanzasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _finanzas_routing_module__WEBPACK_IMPORTED_MODULE_5__["FinanzasPageRoutingModule"]],
      declarations: [_finanzas_page__WEBPACK_IMPORTED_MODULE_6__["FinanzasPage"]]
    })], FinanzasPageModule);
    /***/
  },

  /***/
  "./src/app/pages/finanzas/finanzas.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/pages/finanzas/finanzas.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesFinanzasFinanzasPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZpbmFuemFzL2ZpbmFuemFzLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/pages/finanzas/finanzas.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/finanzas/finanzas.page.ts ***!
    \*************************************************/

  /*! exports provided: FinanzasPage */

  /***/
  function srcAppPagesFinanzasFinanzasPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FinanzasPage", function () {
      return FinanzasPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var chart_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! chart.js */
    "./node_modules/chart.js/dist/Chart.js");
    /* harmony import */


    var chart_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");

    var FinanzasPage = /*#__PURE__*/function () {
      function FinanzasPage(authService, datePipe) {
        _classCallCheck(this, FinanzasPage);

        this.authService = authService;
        this.datePipe = datePipe;
      }

      _createClass(FinanzasPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          console.log('INIT-DINERO');
          this.fechaInicio = '01/01/2020';
          this.fechaFinal = '31/07/2020';
          this.authService.getDinero(this.fechaInicio, this.fechaFinal).then(function (data) {
            _this.recaudacion = data;
          });
          this.authService.getFinanzas('1').then(function (data) {
            _this.recaudacionCantidad = data;
          });
          this.authService.getFinanzas('2').then(function (data) {
            _this.recaudacionDinero = data;
          });
          this.authService.getFinanzas('C').then(function (data) {
            _this.recaudacionDineroCert = data;
          });
          this.authService.getFinanzas('I').then(function (data) {
            _this.recaudacionDineroIns = data;
          });
          console.log('vamos: ' + this.recaudacion);
          this.grafica();
        }
      }, {
        key: "grafica",
        value: function grafica() {
          var _this2 = this;

          this.lineChart = new chart_js__WEBPACK_IMPORTED_MODULE_3__["Chart"](this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
              labels: [],
              datasets: [{
                label: 'Proyección',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [],
                spanGaps: false
              }, {
                label: 'Recaudación Real',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,100,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [],
                spanGaps: false
              }]
            }
          });
          this.fechaInicio = '01/01/2020';
          this.fechaFinal = '31/07/2020';
          this.authService.getDinero(this.fechaInicio, this.fechaFinal).then(function (data) {
            // tslint:disable-next-line:no-unused-expression
            new Promise(function (res) {
              for (var _i = 0, _Object$values = Object.values(data); _i < _Object$values.length; _i++) {
                var elemento = _Object$values[_i];
                console.log('valor: ' + elemento.periodo);

                _this2.lineChart.data.datasets[0].data.push(elemento.proyeccion);

                _this2.lineChart.update();
              }

              res(true);
            });
          });
          this.lineChart.update();
          this.authService.getDinero(this.fechaInicio, this.fechaFinal).then(function (data) {
            // tslint:disable-next-line:no-unused-expression
            new Promise(function (res) {
              for (var _i2 = 0, _Object$values2 = Object.values(data); _i2 < _Object$values2.length; _i2++) {
                var elemento = _Object$values2[_i2];
                console.log('valor: ' + elemento.periodo);

                _this2.lineChart.data.datasets[1].data.push(elemento.recaudado);

                _this2.lineChart.data.labels.push(_this2.datePipe.transform(elemento.periodo, "MMM/yyyy"));

                _this2.lineChart.update();
              }

              res(true);
            });
          });
        }
      }]);

      return FinanzasPage;
    }();

    FinanzasPage.ctorParameters = function () {
      return [{
        type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("lineCanvas", {
      read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
      "static": true
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], FinanzasPage.prototype, "lineCanvas", void 0);
    FinanzasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-finanzas',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./finanzas.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/finanzas/finanzas.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./finanzas.page.scss */
      "./src/app/pages/finanzas/finanzas.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]])], FinanzasPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-finanzas-finanzas-module-es5.js.map