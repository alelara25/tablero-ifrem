function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dinamic-module-dinamic-module-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dinamic-module/dinamic-module.page.html":
  /*!*****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dinamic-module/dinamic-module.page.html ***!
    \*****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesDinamicModuleDinamicModulePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar  style=\"text-align: right;\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"8\">\n        <div>\n          <ion-title></ion-title>\n        </div>\n      </ion-col>\n      <ion-col size=\"4\">\n        <div>\n          <ion-img src=\"assets/ifrem.png\" style=\"margin-right: 5%; height: 35px;\"></ion-img>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-item>\n    <ion-label>Fecha Inicial</ion-label>\n    <!-- <ion-datetime value=\"2019-10-01T15:43:40.394Z\" display-timezone=\"utc\" displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"fechaInicial\"></ion-datetime> -->\n    <ion-datetime min=\"1000-01-01\" max=\"2050-12-01\" displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"fechaInicial\"></ion-datetime>\n  </ion-item>\n  <ion-item>\n    <ion-label>Fecha Final</ion-label>\n    <ion-datetime min=\"1000-01-01\" max=\"2050-12-01\" displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"fechaFinal\"></ion-datetime>\n  </ion-item>\n   <ion-item>\n    <ion-label>Tipo de Gráfica</ion-label>\n    <ion-select [(ngModel)]=\"tipoGrafica\">\n      <ion-select-option value=\"line\">Lineal</ion-select-option>\n      <ion-select-option value=\"bar\">Barra verticales</ion-select-option>\n      <ion-select-option value=\"horizontalBar\">Barras Horizontales</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <!-- <ion-item>\n    <ion-label>Tipo de Gráfica</ion-label>\n    <ion-select [(ngModel)]=\"tipoGrafica\">\n      <ion-select-option value=\"line\">Lineal</ion-select-option>\n      <ion-select-option value=\"bar\">Barra verticales</ion-select-option>\n      <ion-select-option value=\"horizontalBar\">Barras Horizontales</ion-select-option>\n      <ion-select-option value=\"radar\">Radar</ion-select-option>\n      <ion-select-option value=\"pie\">Pastel</ion-select-option>\n      <ion-select-option value=\"polarArea\">Polar</ion-select-option>\n      <ion-select-option value=\"bubble\">burbuja</ion-select-option>\n    </ion-select>\n  </ion-item> -->\n\n    <ion-button type=\"submit\" expand=\"full\" (click)=\"generate()\">Generar Gráfica</ion-button>\n  <div style=\"display: block\">\n    <canvas id=\"chartGraf\" width=\"500\" height=\"400\"></canvas>\n  </div>\n\n  <ion-item *ngIf=\"totFre\">\n    <ion-label color=\"primary\">Total de FRE: <strong> {{totFre}} </strong></ion-label>\n    <!-- <ion-button slot=\"end\" (click)=\"descargarGrafica()\">Descargar</ion-button> -->\n  </ion-item>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/dinamic-module/dinamic-module-routing.module.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/pages/dinamic-module/dinamic-module-routing.module.ts ***!
    \***********************************************************************/

  /*! exports provided: DinamicModulePageRoutingModule */

  /***/
  function srcAppPagesDinamicModuleDinamicModuleRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DinamicModulePageRoutingModule", function () {
      return DinamicModulePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _dinamic_module_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./dinamic-module.page */
    "./src/app/pages/dinamic-module/dinamic-module.page.ts");

    var routes = [{
      path: '',
      component: _dinamic_module_page__WEBPACK_IMPORTED_MODULE_3__["DinamicModulePage"]
    }];

    var DinamicModulePageRoutingModule = function DinamicModulePageRoutingModule() {
      _classCallCheck(this, DinamicModulePageRoutingModule);
    };

    DinamicModulePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], DinamicModulePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/dinamic-module/dinamic-module.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/dinamic-module/dinamic-module.module.ts ***!
    \***************************************************************/

  /*! exports provided: DinamicModulePageModule */

  /***/
  function srcAppPagesDinamicModuleDinamicModuleModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DinamicModulePageModule", function () {
      return DinamicModulePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _dinamic_module_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./dinamic-module-routing.module */
    "./src/app/pages/dinamic-module/dinamic-module-routing.module.ts");
    /* harmony import */


    var _dinamic_module_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./dinamic-module.page */
    "./src/app/pages/dinamic-module/dinamic-module.page.ts");

    var DinamicModulePageModule = function DinamicModulePageModule() {
      _classCallCheck(this, DinamicModulePageModule);
    };

    DinamicModulePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _dinamic_module_routing_module__WEBPACK_IMPORTED_MODULE_5__["DinamicModulePageRoutingModule"]],
      declarations: [_dinamic_module_page__WEBPACK_IMPORTED_MODULE_6__["DinamicModulePage"]]
    })], DinamicModulePageModule);
    /***/
  },

  /***/
  "./src/app/pages/dinamic-module/dinamic-module.page.scss":
  /*!***************************************************************!*\
    !*** ./src/app/pages/dinamic-module/dinamic-module.page.scss ***!
    \***************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesDinamicModuleDinamicModulePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".centrado {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbGFyYS9hcHBzXzIwMjAvTEFCL3YyL3RhYmxlcm8taWZyZW0vc3JjL2FwcC9wYWdlcy9kaW5hbWljLW1vZHVsZS9kaW5hbWljLW1vZHVsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2RpbmFtaWMtbW9kdWxlL2RpbmFtaWMtbW9kdWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kaW5hbWljLW1vZHVsZS9kaW5hbWljLW1vZHVsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2VudHJhZG97XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9IiwiLmNlbnRyYWRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/dinamic-module/dinamic-module.page.ts":
  /*!*************************************************************!*\
    !*** ./src/app/pages/dinamic-module/dinamic-module.page.ts ***!
    \*************************************************************/

  /*! exports provided: DinamicModulePage */

  /***/
  function srcAppPagesDinamicModuleDinamicModulePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DinamicModulePage", function () {
      return DinamicModulePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/alert.service */
    "./src/app/services/alert.service.ts");
    /* harmony import */


    var chart_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! chart.js */
    "./node_modules/chart.js/dist/Chart.js");
    /* harmony import */


    var chart_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");

    var DinamicModulePage = /*#__PURE__*/function () {
      function DinamicModulePage(authService, alertService, datePipe) {
        _classCallCheck(this, DinamicModulePage);

        this.authService = authService;
        this.alertService = alertService;
        this.datePipe = datePipe;
        this.fechaInicial = '';
        this.fechaFinal = '';
        this.totAceptados = 0;
        this.totAutomaticos = 0;
        this.totClg = 0;
        this.totFirmados = 0;
        this.totIngresados = 0;
        this.totOrdinarios = 0;
        this.totPortal = 0;
        this.totRecaudado = 0;
        this.totRechazados = 0;
        this.totSuspendidos = 0;
        this.tipoGrafica = '';
        this.listLabels = ['Aceptados', 'Automáticos', 'CLGs', 'Firmados', 'Ingresados', 'Ordinarios', 'Portal', 'Recaudado', 'Rechazados', 'Suspendidos'];
        this.listTotales = [];
        this.listColors = ['#ffc409'];
      }

      _createClass(DinamicModulePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {// this.showChart();
        }
      }, {
        key: "generate",
        value: function generate() {
          console.log('fecha inicial: ' + this.fechaInicial.toString());
          console.log(this.datePipe.transform(this.fechaInicial, 'dd/MM/yyyy'));
          console.log('Fecha final: ' + this.fechaFinal.toString());

          if (this.fechaFinal !== '' && this.fechaInicial !== '' && this.tipoGrafica !== '') {
            // if (this.fechaFinal !== '' && this.fechaInicial !== '') {
            this.showChart();
          } else {
            this.alertService.presentToast('FALTAN CAMPOS POR SELECIONAR');
          }
        }
      }, {
        key: "showChart",
        value: function showChart() {
          var _this = this;

          var i = -1;
          var colors = this.listLabels.map(function (_) {
            return _this.listColors[i = (i + 1) % _this.listColors.length];
          });

          if (this.chart != null) {
            this.chart.destroy();
          }

          this.ctx = document.getElementById('chartGraf').getContext('2d');
          this.chart = new chart_js__WEBPACK_IMPORTED_MODULE_4__["Chart"](this.ctx, {
            type: this.tipoGrafica,
            data: {
              labels: this.listLabels,
              datasets: []
            },
            options: {
              responsive: true,
              scales: {
                xAxes: [{
                  stacked: true
                }],
                yAxes: [{
                  stacked: true
                }]
              }
            }
          }); // this.addData(this.chart, 'ENERO', '#ffc409', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
          // this.addData(this.chart, 'Febero', '#ef4c29', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);

          this.generateData();
          this.chart.update(); // this.resetValues();

          console.log('COLOR: ' + this.genertaColor());
        }
      }, {
        key: "generateData",
        value: function generateData() {
          var _this2 = this;

          // tslint:disable-next-line:max-line-length
          this.authService.getFechas(this.datePipe.transform(this.fechaInicial, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy')).then(function (data) {
            _this2.datosFecha = data;
            console.log('TOTAL FECHAS: ' + _this2.datosFecha[0].totalFres + ' Total:' + _this2.datosFecha.length);
            console.log(data);
            var monthName = '';
            var monthNameAux = '';
            var contAux = 0;

            _this2.datosFecha.forEach(function (element) {
              contAux++;
              console.log('Contdor: ' + contAux);
              var args = element.fechaCorte.split(' ');
              var argsN = args[0].split('/');
              var fechaNueva = argsN[1] + '/' + argsN[0] + '/' + argsN[2];
              var date = new Date(fechaNueva).toDateString();
              console.log('Fecha in: ' + date);
              date = _this2.datePipe.transform(date, 'MM/dd/yyyy');
              console.log('fecha nueva: ' + date);
              console.log('----------datos-------');
              console.log(_this2.datePipe.transform(date, 'MMMM'));
              console.log(_this2.datePipe.transform(date, 'yyyy'));
              console.log(element.totalAceptados);
              monthName = _this2.datePipe.transform(date, 'MMMM');

              if (monthName === monthNameAux || monthNameAux === '') {
                monthNameAux = _this2.datePipe.transform(date, 'MMMM');
                _this2.totAceptados = _this2.totAceptados + element.totalAceptados;
                _this2.totAutomaticos = _this2.totAutomaticos + element.totalAuto;
                _this2.totClg = _this2.totClg + element.totalCLG;
                _this2.totFirmados = _this2.totFirmados + element.totalFirmados; // this.totFre = 0;

                _this2.totIngresados = _this2.totIngresados + element.totalIngresados;
                _this2.totOrdinarios = _this2.totOrdinarios + element.totalOrdinario;
                _this2.totPortal = _this2.totPortal + element.totalPortal;
                _this2.totRecaudado = _this2.totRecaudado + element.totalRecaudo;
                _this2.totRechazados = _this2.totRechazados + element.totalRechazo;
                _this2.totSuspendidos = _this2.totSuspendidos + element.totalSuspendido;

                if (_this2.datosFecha.length === contAux) {
                  _this2.llenarListaTotales();

                  var randColor = _this2.genertaColor();

                  _this2.addData(_this2.chart, monthNameAux + '/' + _this2.datePipe.transform(date, 'yyyy'), randColor, _this2.listTotales);

                  _this2.resetValues();
                }
              } else {
                _this2.llenarListaTotales();

                var _randColor = _this2.genertaColor();

                _this2.addData(_this2.chart, monthNameAux + '/' + _this2.datePipe.transform(date, 'yyyy'), _randColor, _this2.listTotales);

                monthNameAux = _this2.datePipe.transform(date, 'MMMM');

                _this2.resetValues();

                _this2.totAceptados = _this2.totAceptados + element.totalAceptados;
                _this2.totAutomaticos = _this2.totAutomaticos + element.totalAuto;
                _this2.totClg = _this2.totClg + element.totalCLG;
                _this2.totFirmados = _this2.totFirmados + element.totalFirmados; // this.totFre = 0;

                _this2.totIngresados = _this2.totIngresados + element.totalIngresados;
                _this2.totOrdinarios = _this2.totOrdinarios + element.totalOrdinario;
                _this2.totPortal = _this2.totPortal + element.totalPortal;
                _this2.totRecaudado = _this2.totRecaudado + element.totalRecaudo;
                _this2.totRechazados = _this2.totRechazados + element.totalRechazo;
                _this2.totSuspendidos = _this2.totSuspendidos + element.totalSuspendido;
              }

              _this2.totFre = element.totalFres;
            }); // console.log('Aceptados: ' + this.totAceptados);
            // console.log('Automaticos: ' + this.totAutomaticos);

          });
        }
      }, {
        key: "llenarListaTotales",
        value: function llenarListaTotales() {
          this.listTotales.push(this.totAceptados);
          this.listTotales.push(this.totAutomaticos);
          this.listTotales.push(this.totClg);
          this.listTotales.push(this.totFirmados); // this.listTotales.push(this.totFre);

          this.listTotales.push(this.totIngresados);
          this.listTotales.push(this.totOrdinarios);
          this.listTotales.push(this.totPortal);
          this.listTotales.push(this.totRecaudado);
          this.listTotales.push(this.totRechazados);
          this.listTotales.push(this.totSuspendidos);
        }
      }, {
        key: "addData",
        value: function addData(chart, lab, color, dat) {
          chart.data.datasets.push({
            label: lab,
            backgroundColor: color,
            borderColor: color,
            data: dat,
            fill: false,
            pointStyle: 'rectRot',
            pointRadius: 5,
            pointBorderColor: 'rgb(0, 0, 0)'
          });
          chart.update();
        }
      }, {
        key: "genertaColor",
        value: function genertaColor() {
          var simbolos, color;
          simbolos = '0123456789ABCDEF';
          color = '#';

          for (var i = 0; i < 6; i++) {
            color = color + simbolos[Math.floor(Math.random() * 16)];
          }

          return color;
        }
      }, {
        key: "descargarGrafica",
        value: function descargarGrafica() {}
      }, {
        key: "resetValues",
        value: function resetValues() {
          this.totAceptados = 0;
          this.totAutomaticos = 0;
          this.totClg = 0;
          this.totFirmados = 0;
          this.totFre = 0;
          this.totIngresados = 0;
          this.totOrdinarios = 0;
          this.totPortal = 0;
          this.totRecaudado = 0;
          this.totRechazados = 0;
          this.totSuspendidos = 0;
          this.listTotales = [];
        }
      }]);

      return DinamicModulePage;
    }();

    DinamicModulePage.ctorParameters = function () {
      return [{
        type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
      }, {
        type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]
      }];
    };

    DinamicModulePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-dinamic-module',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./dinamic-module.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dinamic-module/dinamic-module.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./dinamic-module.page.scss */
      "./src/app/pages/dinamic-module/dinamic-module.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]])], DinamicModulePage);
    /***/
  }
}]);
//# sourceMappingURL=pages-dinamic-module-dinamic-module-module-es5.js.map