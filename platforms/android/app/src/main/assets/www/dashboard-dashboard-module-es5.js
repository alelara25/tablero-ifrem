function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDashboardDashboardPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n    <ion-header>\n        <ion-toolbar  color=\"purple\" style=\"text-align: right;\">\n          <ion-buttons slot=\"start\">\n            <ion-menu-button></ion-menu-button>\n          </ion-buttons>\n          <ion-row class=\"ion-align-items-center\">\n            <ion-col size=\"8\">\n              <div>\n                <ion-title></ion-title>\n              </div>\n            </ion-col>\n            <ion-col size=\"4\">\n              <div>\n                <ion-img src=\"assets/ifrem.png\" style=\"margin-right: 5%; height: 35px;\"></ion-img>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-toolbar>\n      </ion-header>\n</ion-header>\n\n\n  <ion-content fullscreen>\n\n    <ion-list>\n      <ion-item-divider>\n        <ion-label>\n          Resumen de los datos IFREM.\n        </ion-label>\n      </ion-item-divider>\n    </ion-list>\n\n\n\n\n\n    <ion-list>\n     <!-- <ion-list-header>List Notes</ion-list-header> -->\n          <ion-item>\n            <ion-label>No. de Folios</ion-label>\n            <ion-note slot=\"end\">{{ totalFres | number }}</ion-note>\n          </ion-item>\n      \n          <ion-item>\n            <ion-label>Trámites Ingresados </ion-label>\n            <ion-note slot=\"end\" color=\"primary\"> {{ totalTramite | number }} </ion-note>\n          </ion-item>\n      \n          <ion-item>\n            <ion-label>Trámites Automatizados</ion-label>\n            <ion-note slot=\"end\" color=\"secondary\"> {{ totalAuto | number }}  </ion-note>\n          </ion-item>\n      \n          <ion-item>\n            <ion-label>Trámites en Línea</ion-label>\n            <ion-note slot=\"end\" color=\"tertiary\">{{ totalLinea | number }} </ion-note>\n          </ion-item>\n\n          <ion-item>\n            <ion-label>Trámites Ordinarios</ion-label>\n            <ion-note slot=\"end\" color=\"primary\">{{ totalOrdinario | number }} </ion-note>\n          </ion-item>\n\n          <ion-item>\n            <ion-label>Trámites CLG</ion-label>\n            <ion-note slot=\"end\" color=\"secondary\">{{ totalClg | number }} </ion-note>\n          </ion-item>\n\n      </ion-list>\n\n      <ion-list>\n        <ion-item-divider>\n          <ion-label>\n            Trámites por Calificación Registral.\n          </ion-label>\n        </ion-item-divider>\n      </ion-list>\n  \n      <ion-list>\n        <!-- <ion-list-header>List Notes</ion-list-header> -->\n             <ion-item>\n               <ion-label>Trámites Firmados</ion-label>\n               <ion-note slot=\"end\">{{ totalFirma | number }}</ion-note>\n             </ion-item>\n         \n             <ion-item>\n               <ion-label>Trámites Aceptados </ion-label>\n               <ion-note slot=\"end\" color=\"primary\"> {{ totalAcptado | number }} </ion-note>\n             </ion-item>\n         \n             <ion-item>\n               <ion-label>Trámites Suspendidos</ion-label>\n               <ion-note slot=\"end\" color=\"secondary\"> {{ totalSusp | number }}  </ion-note>\n             </ion-item>\n\n             <ion-item>\n              <ion-label>Trámites Rechazados</ion-label>\n              <ion-note slot=\"end\" color=\"tertiary\"> {{ totalRechazo | number }}  </ion-note>\n            </ion-item>\n      </ion-list>       \n\n\n\n</ion-content>\n\n";
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: DashboardPageRoutingModule */

  /***/
  function srcAppDashboardDashboardRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPageRoutingModule", function () {
      return DashboardPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _dashboard_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./dashboard.page */
    "./src/app/dashboard/dashboard.page.ts");

    var routes = [{
      path: '',
      component: _dashboard_page__WEBPACK_IMPORTED_MODULE_3__["DashboardPage"]
    }];

    var DashboardPageRoutingModule = function DashboardPageRoutingModule() {
      _classCallCheck(this, DashboardPageRoutingModule);
    };

    DashboardPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], DashboardPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/dashboard/dashboard.module.ts ***!
    \***********************************************/

  /*! exports provided: DashboardPageModule */

  /***/
  function srcAppDashboardDashboardModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function () {
      return DashboardPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./dashboard-routing.module */
    "./src/app/dashboard/dashboard-routing.module.ts");
    /* harmony import */


    var _dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./dashboard.page */
    "./src/app/dashboard/dashboard.page.ts");

    var DashboardPageModule = function DashboardPageModule() {
      _classCallCheck(this, DashboardPageModule);
    };

    DashboardPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardPageRoutingModule"]],
      declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]]
    })], DashboardPageModule);
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/dashboard/dashboard.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppDashboardDashboardPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/dashboard/dashboard.page.ts ***!
    \*********************************************/

  /*! exports provided: DashboardPage */

  /***/
  function srcAppDashboardDashboardPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPage", function () {
      return DashboardPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../services/authentication.service */
    "./src/app/services/authentication.service.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/auth.service */
    "./src/app/services/auth.service.ts");

    var DashboardPage = /*#__PURE__*/function () {
      function DashboardPage(authServiceLog, authService) {
        _classCallCheck(this, DashboardPage);

        this.authServiceLog = authServiceLog;
        this.authService = authService;
      }

      _createClass(DashboardPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          console.log('iniciandoINIT()');
          this.authService.getFres().then(function (data) {
            _this.totalFres = data;
            console.log('TOTAL DE FRES: ' + _this.totalFres);
          });
          this.authService.getTramitesCorrientes().then(function (data) {
            _this.totalTramite = data;
            console.log('TOTAL DE FRES: ' + _this.totalTramite);
          });
          this.authService.getTramitesAuto().then(function (data) {
            _this.totalAuto = data;
            console.log('TOTAL DE FRES: ' + _this.totalAuto);
          });
          this.authService.getTramitesLinea().then(function (data) {
            _this.totalLinea = data;
            console.log('TOTAL DE FRES: ' + _this.totalLinea);
          });
          this.authService.getTramitesOrdinario().then(function (data) {
            _this.totalOrdinario = data;
            console.log('TOTAL DE FRES: ' + _this.totalOrdinario);
          });
          this.authService.getTramitesClg().then(function (data) {
            _this.totalClg = data;
            console.log('TOTAL DE FRES: ' + _this.totalClg);
          });
          this.authService.getTramitesFirmados().then(function (data) {
            _this.totalFirma = data;
            console.log('TOTAL DE FRES: ' + _this.totalFirma);
          });
          this.authService.getTramitesAceptados().then(function (data) {
            _this.totalAcptado = data;
            console.log('TOTAL DE FRES: ' + _this.totalAcptado);
          });
          this.authService.getTramitesSusp().then(function (data) {
            _this.totalSusp = data;
            console.log('TOTAL DE FRES: ' + _this.totalSusp);
          });
          this.authService.getTramitesRechazo().then(function (data) {
            _this.totalRechazo = data;
            console.log('TOTAL DE FRES: ' + _this.totalRechazo);
          });
        }
      }, {
        key: "logoutUser",
        value: function logoutUser() {
          this.authServiceLog.logout();
        }
      }]);

      return DashboardPage;
    }();

    DashboardPage.ctorParameters = function () {
      return [{
        type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
      }];
    };

    DashboardPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-dashboard',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./dashboard.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./dashboard.page.scss */
      "./src/app/dashboard/dashboard.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])], DashboardPage);
    /***/
  }
}]);
//# sourceMappingURL=dashboard-dashboard-module-es5.js.map