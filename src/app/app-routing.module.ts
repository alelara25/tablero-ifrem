
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

/* const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardPageModule',
    canActivate: [AuthGuard]
  }
]; */

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardPageModule',
    canActivate: [AuthGuardService]
  },
  { path: 'mensuales', loadChildren: () => import('./pages/mensuales/mensuales.module').then( m => m.MensualesPageModule) , canActivate: [AuthGuardService] },
  { path: 'anual', loadChildren: () => import('./pages/anual/anual.module').then( m => m.AnualPageModule), canActivate: [AuthGuardService] },
  /*{
    path: 'anual',
    loadChildren: './anual/anual.module#AnualPageModule',
    canActivate: [AuthGuardService]
  },
  */
  {
    path: 'dinamic-module',
    loadChildren: () => import('./pages/dinamic-module/dinamic-module.module').then( m => m.DinamicModulePageModule)
  },
  {
    path: 'finanzas',
    loadChildren: () => import('./pages/finanzas/finanzas.module').then( m => m.FinanzasPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'automatizados',
    loadChildren: () => import('./pages/automatizados/automatizados.module').then( m => m.AutomatizadosPageModule)
  },
  {
    path: 'oficinas',
    loadChildren: () => import('./pages/oficinas/oficinas.module').then( m => m.OficinasPageModule)
  },
  {
    path: 'portales',
    loadChildren: () => import('./pages/portales/portales.module').then( m => m.PortalesPageModule)
  },
  {
    path: 'contacto',
    loadChildren: () => import('./pages/contacto/contacto.module').then( m => m.ContactoPageModule)
  },
  {
    path: 'noticias',
    loadChildren: () => import('./pages/noticias/noticias.module').then( m => m.NoticiasPageModule)
  },
  {
    path: 'quejas',
    loadChildren: () => import('./quejas/quejas.module').then( m => m.QuejasPageModule)
  },
  {
    path: 'modal-about',
    loadChildren: () => import('./pages/modal-about/modal-about.module').then( m => m.ModalAboutPageModule)
  },
  {
    path: 'lineas',
    loadChildren: () => import('./pages/lineas/lineas.module').then( m => m.LineasPageModule)
  },
  {
    path: 'tramites',
    loadChildren: () => import('./pages/tramites/tramites.module').then(m => m.TramitesPageModule)
  },
  {
    path: 'location',
    loadChildren: () => import('./pages/location/location.module').then( m => m.LocationPageModule)
  },
  {
    path: 'tramite-detalle',
    loadChildren: () => import('./pages/tramite-detalle/tramite-detalle.module').then( m => m.TramiteDetallePageModule)
  }

];

/*@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})*/

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
