import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QuejasPage } from './quejas.page';

describe('QuejasPage', () => {
  let component: QuejasPage;
  let fixture: ComponentFixture<QuejasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuejasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QuejasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
