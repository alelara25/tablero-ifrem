import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-quejas',
  templateUrl: './quejas.page.html',
  styleUrls: ['./quejas.page.scss'],
})
export class QuejasPage implements OnInit {

  fechaDeQueja: String = new Date().toISOString();
  usuario: string; 
  texto: string;
  quejas: any; // aqui se recupera la lista de quejas

  constructor(private authService: AuthService){
    this.texto=''; // SE INICIALIZAN ESTOS CAMPOS PARA QUE CADA QUE VEZ QUE INICIE EL PAGE ESTEN VACIOS
    this.usuario='';
    this.fechaDeQueja;
    
    
  }
  
enviar(){
  
    console.log('aqui vamos'); // SLOLO DEBUGEAMOS
    console.log('correo: ' , this.usuario);
    console.log('texto: ' , this.texto);
    console.log('fecha: ' , this.fechaDeQueja);

    var datos = {usuario: this.usuario, texto: this.texto, fechaDeQueja: this.fechaDeQueja};
  this.authService.postQueja(datos).subscribe((data) => {this.quejas=JSON.stringify (data);
  
  })
    //this.authService.postQueja()
      //.then( data => {
        // this.quejas = data; 
   // }); 
        //console.log('quejas: ' , this.quejas); 
  
  }
  
  ngOnInit() {
  }

}
