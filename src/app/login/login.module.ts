import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { ModalIfremPage } from '../pages/modals/modal-ifrem/modal-ifrem.page';
import { ModalMisionPage } from '../pages/modals/modal-mision/modal-mision.page';
import { ModalSeguridadPage } from '../pages/modals/modal-seguridad/modal-seguridad.page';
import { ModalCalidadPage } from '../pages/modals/modal-calidad/modal-calidad.page';
import { ModalIfremPageModule } from '../pages/modals/modal-ifrem/modal-ifrem.module';
import { ModalCalidadPageModule } from '../pages/modals/modal-calidad/modal-calidad.module';
import { ModalMisionPageModule } from '../pages/modals/modal-mision/modal-mision.module';
import { ModalSeguridadPageModule } from '../pages/modals/modal-seguridad/modal-seguridad.module';

@NgModule({
  entryComponents: [
    ModalIfremPage,
    ModalMisionPage,
    ModalSeguridadPage,
    ModalCalidadPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,
    ModalIfremPageModule,
    ModalCalidadPageModule,
    ModalSeguridadPageModule,
    ModalMisionPageModule
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
