import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { HttpClient } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';
import {AuthService} from '../services/auth.service';
import {Ventana} from '../models/user';
import { ModalController } from '@ionic/angular';
import { ModalIfremPage } from '../pages/modals/modal-ifrem/modal-ifrem.page';
import { ModalMisionPage } from '../pages/modals/modal-mision/modal-mision.page';
import { ModalCalidadPage } from '../pages/modals/modal-calidad/modal-calidad.page';
import { ModalSeguridadPage } from '../pages/modals/modal-seguridad/modal-seguridad.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario: string;
  passwd: any;
  resultado: any;
  estado: Ventana;

  constructor(
    private authService: AuthenticationService,
    public httpClient: HttpClient,
    private authServiceAll: AuthService,
    private modalCtrl: ModalController
     ) { 
       this.usuario ='';
       this.passwd='';
     }

  ngOnInit() {

      this.authServiceAll.getEstado().subscribe((res) => {
          console.log('check-usada: ' , res);
          this.estado = res;
      });
  }

  async abrirIfrem() {
    const modal = await this.modalCtrl.create({
     component: ModalIfremPage
    });
 
    await modal.present();
   }
 
   async abrirMision() {
     const modal = await this.modalCtrl.create({
      component: ModalMisionPage
     });
     await modal.present();
    }
 
    async abrirCalidad() {
     const modal = await this.modalCtrl.create({
      component: ModalCalidadPage
     });
     await modal.present();
    }
 
    async abrirSeguridad() {
     const modal = await this.modalCtrl.create({
      component: ModalSeguridadPage
     });
     await modal.present();
    }

  loginUser(){

    //this.authService.login();
      this.postDatos(this.usuario, this.passwd).then( data => {
        this.resultado = data;
        console.log('resultado: ' + this.resultado);
        this.authService.login();
      });
  }

  postDatos(usuer: string, password: string){
    let datos = { nombre:'alara',email:'alelara25@gmail.com'}
    let options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
   console.log('aqui vamos');
    let hash= CryptoJS.SHA256(password);
    console.log('ORIGINAL:8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92');
    console.log('ORIGINAL:' + hash);

    //var url = 'http://10.8.6.28:8085/api/v1/servicios/login?email=' + usuer + '&password=8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92';
    var url = 'https://msconsulta.ifrem.gob.mx/api/v1/servicios/login?email=' + usuer + '&password=' + hash;
   
   return new Promise(resolve => {
    this.httpClient.post(url,JSON.stringify(datos),options)
       .subscribe(data => {
         console.log("lara: " + data);
         resolve(data);
        },
        error => {
          console.log('oops', error);
          alert('ERROR! CREDENCIALES INVALIDAS!');
        });
   });
 
  }

}
