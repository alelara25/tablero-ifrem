export class User {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
}

export class Ventana {
    estado: boolean;
}
export class Metricas {

    fechaCorte: Date;
    totalAceptados: string;
    totalAuto: string;
    totalClg: string;
    totalFirmados: string;
    totalFres: string;
    totalIngresados: string;
    totalOrdinario: string;
    totalPortal: string;
    totalRecaudo: string;
    totalRechazo: string;
    totalSusp: string;

}

export interface Tramite {
    id: string;
    numeroDeTramite: string;
    fechaDeCracion: any;
    fechaFirma: any;
    fechaEntrega: any;
    statusDeTramite: statusDeTramite;
    tipoResultadoDeCalificacion: tipoResultadoDeCalificacion;
    notario?: string;
    rec?: string;
    oficinaRegistral: oficinaRegistral;
}

export interface statusDeTramite {
    id: string;
    version: string;
    descripcion: string;
    nombre: string;
}

export interface tipoResultadoDeCalificacion {
    id: string;
    version: string;
    descripcion: string;
    nombre:string;
}

export interface oficinaRegistral {
    id: string;
    version: string;
    descripcion: string;
    habilitada: boolean;
}


