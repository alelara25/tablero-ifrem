import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Chart } from 'chart.js';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-finanzas',
  templateUrl: './finanzas.page.html',
  styleUrls: ['./finanzas.page.scss'],
})
export class FinanzasPage implements OnInit {

  fechaInicio: any;
  fechaFinal: any;
  recaudacion: any;
  recaudacionCantidad: any;
  recaudacionDinero: any;
  recaudacionDineroCert: any;
  recaudacionDineroIns: any;
  recaudacionBancos: any;
  recaudacionBancosRecaudado: any;
  index2: 1;

  lineChart: any;
  @ViewChild("lineCanvas", { read: ElementRef, static: true }) private lineCanvas: ElementRef;


  lineCanvasBancos: any;
  @ViewChild("lineCanvasBancos", { read: ElementRef, static: true }) private barChart: ElementRef;

  lineCanvasBancosReca: any;
  @ViewChild("lineCanvasBancosReca", { read: ElementRef, static: true }) private barChartBanco: ElementRef;

  constructor(private authService: AuthService, private datePipe: DatePipe) { }


  ngOnInit() {

    console.log('INIT-DINERO');
    this.fechaInicio = '01/01/2020';
    this.fechaFinal = '31/12/2020';

    this.authService.getDinero(this.fechaInicio, this.fechaFinal)
    .then( data => {
      this.recaudacion = data;
    });

    this.authService.getFinanzas('1')
    .then( data => {
      this.recaudacionCantidad = data;
    });

    this.authService.getFinanzas('2')
    .then( data => {
      this.recaudacionDinero = data;
    });

    this.authService.getFinanzas('C')
    .then( data => {
      this.recaudacionDineroCert = data;
    });

    this.authService.getFinanzas('I')
    .then( data => {
      this.recaudacionDineroIns = data;
    });

    this.authService.getBancos()
    .then( data => {
      this.recaudacionBancos = data;
    });


    console.log('vamos: ' + this.recaudacion);

    this.grafica();

  }

  public grafica() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            label: 'Proyección',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
            spanGaps: false,
          },
          {
            label: 'Recaudación Real',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,100,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
            spanGaps: false,
          }
        ]
      }
    });

    this.lineCanvasBancos = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          label: 'Transacciones',
          data: [],
          backgroundColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(38, 194, 129)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    this.lineCanvasBancosReca = new Chart(this.barChartBanco.nativeElement, {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          label: 'Recaudación',
          data: [],
          backgroundColor: 'rgba(75,192,192,0.4)', // array should have same number of elements as number of dataset
          borderColor: 'rgba(75,192,192,1)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    

    this.fechaInicio = '01/01/2020';
    this.fechaFinal = '31/12/2020';


    this.authService.getDinero(this.fechaInicio, this.fechaFinal).then(data => {
      // tslint:disable-next-line:no-unused-expression
      new Promise<boolean>((res) => {
        for (let elemento of Object.values(data)) {
          console.log('valor: ' + elemento.periodo);
          this.lineChart.data.datasets[0].data.push(elemento.proyeccion);
          this.lineChart.update();
        }
        res(true);
      });
    });

    this.lineCanvasBancos.update();
    this.authService.getBancos().then(data => {
      // tslint:disable-next-line:no-unused-expression
      new Promise<boolean>((res) => {
        for (let elemento of Object.values(data)) {
          console.log('valor: ' + elemento.centroPago);
          this.lineCanvasBancos.data.datasets[0].data.push(elemento.cantidad);
          this.lineCanvasBancos.data.labels.push(elemento.centroPago);
          this.lineCanvasBancos.update();
        }
        res(true);
      });
    });

    this.lineCanvasBancosReca.update();
    this.authService.getBancos().then(data => {
      // tslint:disable-next-line:no-unused-expression
      new Promise<boolean>((res) => {
        for (let elemento of Object.values(data)) {
          console.log('valor: ' + elemento.centroPago);
          this.lineCanvasBancosReca.data.datasets[0].data.push(elemento.recaudado);
          this.lineCanvasBancosReca.data.labels.push(elemento.centroPago);
          this.lineCanvasBancosReca.update();
        }
        res(true);
      });
    });

    this.lineChart.update();
    this.authService.getDinero(this.fechaInicio, this.fechaFinal).then(data => {
      // tslint:disable-next-line:no-unused-expression
      new Promise<boolean>((res) => {
        for (let elemento of Object.values(data)) {
          console.log('valor: ' + elemento.periodo);
          this.lineChart.data.datasets[1].data.push(elemento.recaudado);
          this.lineChart.data.labels.push(this.datePipe.transform(elemento.periodo,"MM/yy"));
          this.lineChart.update();

        }
        res(true);
      });
    });

  }
}
