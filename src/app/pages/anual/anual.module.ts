import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnualPageRoutingModule } from './anual-routing.module';

import { AnualPage } from './anual.page';

import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [
    NgxChartsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    AnualPageRoutingModule
  ],
  declarations: [AnualPage]
})
export class AnualPageModule {}
