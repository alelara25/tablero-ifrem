import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AnualService } from 'src/app/services/anual.service';
import { DatePipe } from '@angular/common';
import { Chart } from 'chart.js';
import { LoadingController } from '@ionic/angular';
import { meses } from 'src/app/models/meses';

@Component({
  selector: 'app-anual',
  templateUrl: './anual.page.html',
  styleUrls: ['./anual.page.scss'],
})
export class AnualPage implements OnInit {

  lineChart: any;
  @ViewChild("lineCanvas", { read: ElementRef, static: true }) private lineCanvas: ElementRef;

  barChart: any;
  @ViewChild("barCanvas", { read: ElementRef, static: true }) private barCanvas: ElementRef;

  lineAuto: any;
  @ViewChild("autoCanvas", { read: ElementRef, static: true }) private autoCanvas: ElementRef;

  meses: any[];
  view: any[] = [750, 300];
  datosThisYear: any;
  datosPastYear: any;
  date = new Date();
  loading: any;
  segmentModel = "Aceptados";
  pos2019 = 0;
  pos2020 = 1;

  constructor(private anualService: AnualService,
    private datePipe: DatePipe,
    public loadingCtrl: LoadingController) {
    Object.assign(this, { meses });
    //this.messageLoading('Generando Gráficas');
  }

  ngOnInit() {
    this.generarGrafica();
    this.generarGraficaBarras();
    this.generarGraficaAuto();
    this.etiquetas();
    for (var i = 0; i <= 1; i++) {
      this.obtenerDatos(i);
    }
  }

  public generarGrafica() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            label: 'Total de CLG\'s generados ' + this.datePipe.transform(new Date(this.date.getFullYear(), 0, 1), 'yyyy'),
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
            spanGaps: false,
          },
          {
            label: 'Total de CLG\'s generados ' + this.datePipe.transform(new Date(this.date.getFullYear() - 1, 0, 1), 'yyyy'),
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,100,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
            spanGaps: false,
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              display: false
            }
          }]
        }
      }
    });
  }

  public generarGraficaBarras() {
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: [],
        datasets: [
          {
            label: 'Aceptados ' + this.datePipe.transform(new Date(this.date.getFullYear() - 1, 0, 1), 'yyyy'),
            data: [],
            backgroundColor: 'rgba(48, 152, 6, 0.5)'
          },
          {
            label: 'Aceptados ' + this.datePipe.transform(new Date(this.date.getFullYear(), 0, 1), 'yyyy'),
            data: [],
            backgroundColor: 'rgba(48, 152, 6, 1)'
          },
          {
            label: 'Suspendidos ' + this.datePipe.transform(new Date(this.date.getFullYear() - 1, 0, 1), 'yyyy'),
            data: [],
            backgroundColor: 'rgba(211, 180, 9, 0.5)'
          },
          {
            label: 'Suspendidos ' + this.datePipe.transform(new Date(this.date.getFullYear(), 0, 1), 'yyyy'),
            data: [],
            backgroundColor: 'rgba(211, 180, 9, 1)'
          },
          {
            label: 'Rechazados ' + this.datePipe.transform(new Date(this.date.getFullYear() - 1, 0, 1), 'yyyy'),
            data: [],
            backgroundColor: 'rgba(155, 23, 23, 0.5)'
          },
          {
            label: 'Rechazados ' + this.datePipe.transform(new Date(this.date.getFullYear(), 0, 1), 'yyyy'),
            data: [],
            backgroundColor: 'rgba(155, 23, 23, 1)'
          }]
      },
      options: {
        legend: {
          fullWidth: true,
          labels: {
            fontSize: 10
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              display: false
            }
          }]
        }
      }
    });
  }

  public generarGraficaAuto() {
    this.lineAuto = new Chart(this.autoCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            label: 'Trámites automatizados ' + this.datePipe.transform(new Date(this.date.getFullYear(), 0, 1), 'yyyy'),
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
            spanGaps: false,
          },
          {
            label: 'Trámites automatizados ' + this.datePipe.transform(new Date(this.date.getFullYear() - 1, 0, 1), 'yyyy'),
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,100,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
            spanGaps: false,
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              display: false
            }
          }]
        }
      }
    });
  }

  public etiquetas() {
    for (let element of this.meses) {
      this.lineChart.data.labels.push(element.mes);
      this.barChart.data.labels.push(element.mes);
      this.lineAuto.data.labels.push(element.mes);
    }
    this.lineChart.update();
  }

  public obtenerDatos(i: number) {
    var params: string;
    var primerDia = this.datePipe.transform(new Date(this.date.getFullYear() - i, 0, 1), 'dd/MM/yyyy');
    var ultimoDia = this.datePipe.transform(new Date(this.date.getFullYear() - i, 11 + 1, 0), 'dd/MM/yyyy');
    params = '?fechaInicial=' + primerDia + '&fechaFinal=' + ultimoDia;
    this.anualService.getData(params).then(data => {
      if (i == 0) {
        this.datosThisYear = data;
      } else {
        this.datosPastYear = data;
      }
    })
      .then(() => {
        if (this.datosPastYear !== undefined && this.datosThisYear !== undefined) {
          this.llenarTotales();
        }
      });
  }

  public llenarTotales() {
    let totalCLG = 0;
    let totalAceptados = 0;
    let totalSuspendidos = 0;
    let totalRechazados = 0;
    let totalAutomatizados = 0;
    let aux = 1;
    this.datosThisYear.forEach(element => {
      if (element.fechaCorte.split("/")[0] == 31 && element.fechaCorte.split("/")[1] == 12) {
        totalCLG = totalCLG + element.totalCLG;
        totalAceptados = totalAceptados + element.totalAceptados;
        totalSuspendidos = totalSuspendidos + element.totalSuspendido;
        totalRechazados = totalRechazados + element.totalRechazo;
        totalAutomatizados = totalAutomatizados + element.totalAuto;
        this.lineChart.data.datasets[0].data.push(totalCLG);
        this.lineChart.update();
        this.barChart.data.datasets[1].data.push(totalAceptados);
        this.barChart.data.datasets[3].data.push(totalSuspendidos);
        this.barChart.data.datasets[5].data.push(totalRechazados);
        this.barChart.update();
        this.lineAuto.data.datasets[0].data.push(totalAutomatizados);
        this.lineAuto.update();
      } else if (element.fechaCorte.split("/")[1] == aux) {
        totalCLG = totalCLG + element.totalCLG;
        totalAceptados = totalAceptados + element.totalAceptados;
        totalSuspendidos = totalSuspendidos + element.totalSuspendido;
        totalRechazados = totalRechazados + element.totalRechazo;
        totalAutomatizados = totalAutomatizados + element.totalAuto;
      } else {
        this.lineChart.data.datasets[0].data.push(totalCLG);
        this.lineChart.update();
        this.barChart.data.datasets[1].data.push(totalAceptados);
        this.barChart.data.datasets[3].data.push(totalSuspendidos);
        this.barChart.data.datasets[5].data.push(totalRechazados);
        this.barChart.update();
        this.lineAuto.data.datasets[0].data.push(totalAutomatizados);
        this.lineAuto.update();
        totalCLG = 0 + element.totalCLG;
        totalAceptados = 0 + element.totalAceptados;
        totalSuspendidos = 0 + element.totalSuspendido;
        totalRechazados = 0 + element.totalRechazo;
        totalAutomatizados = 0 + element.totalAuto;
        aux++;
      }
    });
    totalCLG = 0;
    totalAceptados = 0;
    totalSuspendidos = 0;
    totalRechazados = 0;
    totalAutomatizados = 0;
    aux = 1;
    this.datosPastYear.forEach(element => {
      if (element.fechaCorte.split("/")[0] == 31 && element.fechaCorte.split("/")[1] == 12) {
        totalCLG = totalCLG + element.totalCLG;
        totalAceptados = totalAceptados + element.totalAceptados;
        totalSuspendidos = totalSuspendidos + element.totalSuspendido;
        totalRechazados = totalRechazados + element.totalRechazo;
        totalAutomatizados = totalAutomatizados + element.totalAuto;
        this.lineChart.data.datasets[1].data.push(totalCLG);
        this.lineChart.update();
        this.barChart.data.datasets[0].data.push(totalAceptados);
        this.barChart.data.datasets[2].data.push(totalSuspendidos);
        this.barChart.data.datasets[4].data.push(totalRechazados);
        this.barChart.update();
        this.lineAuto.data.datasets[1].data.push(totalAutomatizados);
        this.lineAuto.update();
      } else if (element.fechaCorte.split("/")[1] == aux) {
        totalCLG = totalCLG + element.totalCLG;
        totalAceptados = totalAceptados + element.totalAceptados;
        totalSuspendidos = totalSuspendidos + element.totalSuspendido;
        totalRechazados = totalRechazados + element.totalRechazo;
        totalAutomatizados = totalAutomatizados + element.totalAuto;
      } else {
        this.lineChart.data.datasets[1].data.push(totalCLG);
        this.lineChart.update();
        this.barChart.data.datasets[0].data.push(totalAceptados);
        this.barChart.data.datasets[2].data.push(totalSuspendidos);
        this.barChart.data.datasets[4].data.push(totalRechazados);
        this.barChart.update();
        this.lineAuto.data.datasets[1].data.push(totalAutomatizados);
        this.lineAuto.update();
        totalCLG = 0 + element.totalCLG;
        totalAceptados = 0 + element.totalAceptados;
        totalSuspendidos = 0 + element.totalSuspendido;
        totalRechazados = 0 + element.totalRechazo;
        totalAutomatizados = 0 + element.totalAuto;
        aux++;
      }
    });
    //this.loading.dismiss();
  }

  async messageLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  segmentChanged(event) {
    switch (this.segmentModel) {
      case 'Aceptados': {
        this.pos2019 = 0;
        this.pos2020 = 1;
        //statements 
        console.log("Entrando a aceptados");
        console.log(this.segmentModel);
        break;
      }
      case 'Suspendidos': {
        this.pos2019 = 2;
        this.pos2020 = 3;
        //statements 
        console.log("Entrando a suspendidos");
        console.log(this.segmentModel);
        break;
      }
      case 'Rechazados': {
        this.pos2019 = 4;
        this.pos2020 = 5;
        //statements 
        console.log("Entrando a rechzados");
        console.log(this.segmentModel);
        break;
      }
      default: {
        //statements 
        console.log(this.segmentModel);
        console.log(this.segmentModel);
        break;
      }
    }
    console.log(event);
  }
}