import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AnualPage } from './anual.page';

describe('AnualPage', () => {
  let component: AnualPage;
  let fixture: ComponentFixture<AnualPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnualPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AnualPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
