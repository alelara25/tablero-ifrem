import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TramiteDetallePage } from './tramite-detalle.page';

describe('TramiteDetallePage', () => {
  let component: TramiteDetallePage;
  let fixture: ComponentFixture<TramiteDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TramiteDetallePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TramiteDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
