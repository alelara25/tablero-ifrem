import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {Tramite} from '../../models/user';

@Component({
  selector: 'app-tramite-detalle',
  templateUrl: './tramite-detalle.page.html',
  styleUrls: ['./tramite-detalle.page.scss'],
})
export class TramiteDetallePage implements OnInit {

  @Input() tramiteDetail: Tramite;

  constructor(public modalCtrl: ModalController) { }

  ngOnInit(): void {
    console.log(this.tramiteDetail);
  }

  public dismissModal() {
    if (this.modalCtrl) {
      this.modalCtrl.dismiss().then(() => { this.modalCtrl = null; });
    }
  }

}
