import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TramiteDetallePageRoutingModule } from './tramite-detalle-routing.module';

import { TramiteDetallePage } from './tramite-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TramiteDetallePageRoutingModule
  ],
  declarations: [TramiteDetallePage]
})
export class TramiteDetallePageModule {}
