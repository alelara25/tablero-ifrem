import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinamicModulePage } from './dinamic-module.page';

const routes: Routes = [
  {
    path: '',
    component: DinamicModulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinamicModulePageRoutingModule {}
