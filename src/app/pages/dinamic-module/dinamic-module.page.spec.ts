import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinamicModulePage } from './dinamic-module.page';

describe('DinamicModulePage', () => {
  let component: DinamicModulePage;
  let fixture: ComponentFixture<DinamicModulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinamicModulePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinamicModulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
