import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinamicModulePageRoutingModule } from './dinamic-module-routing.module';

import { DinamicModulePage } from './dinamic-module.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinamicModulePageRoutingModule
  ],
  declarations: [DinamicModulePage]
})
export class DinamicModulePageModule {}
