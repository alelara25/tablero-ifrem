import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { Chart } from 'chart.js';
import { DatePipe } from '@angular/common';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-dinamic-module',
  templateUrl: './dinamic-module.page.html',
  styleUrls: ['./dinamic-module.page.scss'],
})
export class DinamicModulePage implements OnInit {

  informacionLoc: string;
  datosFecha: any;
  fechaInicial = '';
  fechaFinal = '';
  loading: any;

  totAceptados: any = 0;
  totAutomaticos: any = 0;
  totClg: any = 0;
  totFirmados: any = 0;
  totFre: any;
  totIngresados: any = 0;
  totOrdinarios: any = 0;
  totPortal: any = 0;
  totRecaudado: any = 0;
  totRechazados: any = 0;
  totSuspendidos: any = 0;

  tipoGrafica = '';
  chart: any;
  ctx: any;

  listLabels: Array<any> = [
    'Aceptados',
    'Automatizados',
    'CLG',
    'Firmados',
    'Ingresados',
    'Ordinarios',
    'Portal',
    'Rechazados',
    'Suspendidos'];

    listTotales: Array<any> = [];
    listResult: Array<any> = [];
    listGrid: Array<any> = [];
    listHead: Array<string> = [];
    listBody: Array<any> = [];
    listColors = [
      '#ffc409'
    ];

  constructor(private authService: AuthService,
              private alertService: AlertService,
              private datePipe: DatePipe,
              public loadingCtrl: LoadingController) { }
  ngOnInit() {
    // this.showChart();
  }

  generate() {
    console.log('fecha inicial: ' + this.fechaInicial.toString());
    console.log(this.datePipe.transform(this.fechaInicial, 'dd/MM/yyyy'));
    console.log('Fecha final: ' + this.fechaFinal.toString());
    if (this.fechaFinal !== '' && this.fechaInicial !== '' && this.tipoGrafica !== '') {
    // if (this.fechaFinal !== '' && this.fechaInicial !== '') {
    this.showChart();
   } else {
    this.alertService.presentToast('FALTAN CAMPOS POR SELECIONAR');
   }
  }

  showChart() {
    let i = -1;
    const colors = this.listLabels.map(_ => this.listColors[(i = (i + 1) % this.listColors.length)]);
    if (this.chart != null) {
     this.chart.destroy();
    }
    if (this.tipoGrafica !== 'horizontalBar') {
    this.ctx = (document.getElementById('chartGraf') as any).getContext('2d');
    this.chart = new Chart(this.ctx, {
        type: this.tipoGrafica,
        data: {
        labels: this.listLabels,
        datasets: []
       },
       options: {
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true,
                ticks: {
                      callback(value, index, values) {
                        return value / 1000 + 'k';
                    }
                  }
            }]
        }
    }
    });
  } else {
    this.ctx = (document.getElementById('chartGraf') as any).getContext('2d');
    this.chart = new Chart(this.ctx, {
        type: this.tipoGrafica,
        data: {
        labels: this.listLabels,
        datasets: []
       },
       options: {
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                  callback(value, index, values) {
                    return value / 1000 + 'k';
                }
              }
            }],
            yAxes: [{
                stacked: true,
            }]
        }
    }
    });
  }

    // this.addData(this.chart, 'ENERO', '#ffc409', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
    // this.addData(this.chart, 'Febero', '#ef4c29', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
    this.generateData();
    this.chart.update();
    // this.resetValues();
    console.log('COLOR: ' + this.genertaColor());
  }

  generateData() {
    this.listHead = [];
    this.listBody = [];
    this.listBody.push(this.listLabels);
    this.messageLoading('Generando Gráfica');
    // tslint:disable-next-line:max-line-length
    this.authService.getFechas(this.datePipe.transform(this.fechaInicial, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy')).then(data => {
      if (data.toString() !== '') {
      console.log('Respuesta: ' + data);
      this.datosFecha = data;
      console.log ('TOTAL FECHAS: ' + this.datosFecha[0].totalFres + ' Total:' + this.datosFecha.length);
      console.log (data);

      let monthName = '';
      let monthNameAux = '';
      let contAux = 0;
      let mes = '';
      this.datosFecha.forEach(element => {
        contAux ++;
        console.log('Contdor: ' + contAux);
        const args = element.fechaCorte.split(' ');
        const argsN = args[0].split('/');
        const fechaNueva = argsN[1] + '/' + argsN[0] + '/' + argsN[2];
        let date = new Date(fechaNueva).toDateString();
        console.log('Fecha in: ' + date);
        date = this.datePipe.transform(date, 'MM/dd/yyyy');
        console.log('fecha nueva: ' + date);
        console.log('----------datos-------');
        console.log(this.datePipe.transform(date, 'MMMM'));
        console.log(this.datePipe.transform(date, 'yyyy'));
        console.log(element.totalAceptados);
        monthName = this.datePipe.transform(date, 'MMMM');

        if ((monthName === monthNameAux) || (monthNameAux === '')) {
          monthNameAux = this.datePipe.transform(date, 'MMMM');
          this.totAceptados = this.totAceptados + element.totalAceptados;
          this.totAutomaticos = this.totAutomaticos + element.totalAuto;
          this.totClg = this.totClg + element.totalCLG;
          this.totFirmados = this.totFirmados + element.totalFirmados;
          // this.totFre = 0;
          this.totIngresados = this.totIngresados + element.totalIngresados;
          this.totOrdinarios = this.totOrdinarios + element.totalOrdinario;
          this.totPortal = this.totPortal + element.totalPortal;
          // this.totRecaudado = this.totRecaudado + element.totalRecaudo;
          this.totRechazados = this.totRechazados + element.totalRechazo;
          this.totSuspendidos = this.totSuspendidos + element.totalSuspendido;

          if (this.datosFecha.length === contAux) {
            this.llenarListaTotales();
            const randColor = this.genertaColor();
            mes = this.obtenerMes(monthNameAux) + ', ' + this.datePipe.transform(date, 'yy');
            this.listHead.push(mes);
            this.listBody.push(this.listTotales);
            // this.listGrid.push(this.listResult);
            // console.log('GRID: ' + this.listGrid);
            this.addData(this.chart, mes, randColor, this.listTotales);
            // this.listResult = [];
            this.resetValues();
           }
         } else {
          this.llenarListaTotales();
          const randColor = this.genertaColor();
          mes = this.obtenerMes(monthNameAux) + ', ' + this.datePipe.transform(date, 'yy');
          this.listHead.push(mes);
          this.listBody.push(this.listTotales);
          // this.listGrid.push(this.listResult);
          // console.log('GRID: ' + this.listGrid);
          this.addData(this.chart, mes, randColor, this.listTotales);
          monthNameAux = this.datePipe.transform(date, 'MMMM');
          // this.listResult = [];
          this.resetValues();

          this.totAceptados = this.totAceptados + element.totalAceptados;
          this.totAutomaticos = this.totAutomaticos + element.totalAuto;
          this.totClg = this.totClg + element.totalCLG;
          this.totFirmados = this.totFirmados + element.totalFirmados;
          // this.totFre = 0;
          this.totIngresados = this.totIngresados + element.totalIngresados;
          this.totOrdinarios = this.totOrdinarios + element.totalOrdinario;
          this.totPortal = this.totPortal + element.totalPortal;
          // this.totRecaudado = this.totRecaudado + element.totalRecaudo;
          this.totRechazados = this.totRechazados + element.totalRechazo;
          this.totSuspendidos = this.totSuspendidos + element.totalSuspendido;
         }
        this.totFre = element.totalFres;
      });

      console.log('Total head: ' + this.listHead.length);
      // console.log('Automaticos: ' + this.totAutomaticos);
      // this.loading.dismiss();
    } else { this.alertService.presentToast('NO HAY DATOS PARA MOSTRAR'); }
    });
  }

  obtenerMes(mes: string) {
    switch (mes) {
      case 'January': return 'Ene';
      case 'February': return 'Feb';
      case 'March': return 'Mar';
      case 'April': return 'Abr';
      case 'May': return 'May';
      case 'June': return 'Jun';
      case 'July': return 'Jul';
      case 'August': return 'Ago';
      case 'September': return 'Sep';
      case 'October': return 'Oct';
      case 'November': return 'Nov';
      case 'December': return 'Dic';
    }
  }

  llenarListaTotales() {
    this.listTotales.push(this.totAceptados);
    this.listTotales.push(this.totAutomaticos);
    this.listTotales.push(this.totClg);
    this.listTotales.push(this.totFirmados);
    // this.listTotales.push(this.totFre);
    this.listTotales.push(this.totIngresados);
    this.listTotales.push(this.totOrdinarios);
    this.listTotales.push(this.totPortal);
    // this.listTotales.push(this.totRecaudado);
    this.listTotales.push(this.totRechazados);
    this.listTotales.push(this.totSuspendidos);
  }

  addData(chart, lab, color, dat) {
    chart.data.datasets.push({
    label: lab,
    backgroundColor: color,
    borderColor: color,
    data: dat,
    fill: false,
    pointStyle: 'rectRot',
    pointRadius: 5,
    pointBorderColor: 'rgb(0, 0, 0)'
    });
    chart.update();
  }

  genertaColor() {
    let simbolos, color;
    simbolos = '0123456789ABCDEF';
    color = '#';

    for (let i = 0; i < 6; i++) {
      color = color + simbolos[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  descargarGrafica() {
  }

  async messageLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
     message
    });
    this.loading.present();

    setTimeout(() => {
      this.loading.dismiss();
    }, 2000);
  }

  resetValues() {
    this.totAceptados = 0;
    this.totAutomaticos = 0;
    this.totClg = 0;
    this.totFirmados = 0;
    this.totFre = 0;
    this.totIngresados = 0;
    this.totOrdinarios = 0;
    this.totPortal = 0;
    this.totRecaudado = 0;
    this.totRechazados = 0;
    this.totSuspendidos = 0;
    this.listTotales = [];
  }

}
