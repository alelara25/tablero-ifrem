import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ModalController} from '@ionic/angular';
import { Tramite } from 'src/app/models/user';
import {TramiteDetallePage} from '../tramite-detalle/tramite-detalle.page';
import {readTsconfig} from '@angular-devkit/build-angular/src/angular-cli-files/utilities/read-tsconfig';

@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.page.html',
  styleUrls: ['./tramites.page.scss'],
})
export class TramitesPage implements OnInit {



  tramite: string;
  oficinas: any;
  oficina: any;
  tramiteDetail: any;
  showProgress: boolean;

  constructor(private authService: AuthService, private modalController: ModalController) {
    this.showProgress = false;

  }

  buscarTramite() {
    this.showProgress = true;
    this.authService.getTramite(this.tramite, this.oficina.id)
        .then(data => {
          this.tramiteDetail = data;
          console.log('vamos por ws');
          this.mostrarTramite(this.tramiteDetail);
          console.log('Entrando a buscar Tramite', this.tramite, this.oficina, this.tramiteDetail)
        })
        .finally(() => {
          this.showProgress = false;
        });
  }

  async mostrarTramite(tramiteDetail: Tramite) {
    const modal = await this.modalController.create({
      component: TramiteDetallePage,
      componentProps: { tramiteDetail }
    });
    return await modal.present();
  }



  ngOnInit() {
      this.authService.getOficinas()
          .then( data => {
              this.oficinas = data;
          });
  }
}
