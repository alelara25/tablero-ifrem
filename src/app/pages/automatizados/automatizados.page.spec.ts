import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AutomatizadosPage } from './automatizados.page';

describe('AutomatizadosPage', () => {
  let component: AutomatizadosPage;
  let fixture: ComponentFixture<AutomatizadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomatizadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AutomatizadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
