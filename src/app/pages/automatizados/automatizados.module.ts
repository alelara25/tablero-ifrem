import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AutomatizadosPageRoutingModule } from './automatizados-routing.module';

import { AutomatizadosPage } from './automatizados.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AutomatizadosPageRoutingModule
  ],
  declarations: [AutomatizadosPage]
})
export class AutomatizadosPageModule {}
