import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-automatizados',
  templateUrl: './automatizados.page.html',
  styleUrls: ['./automatizados.page.scss'],
})
export class AutomatizadosPage implements OnInit {

  automatizados: any;
  servicios: any;
  fechaInicio: any;
  fechaFinal: any;
  tipo: any;
  servicioPeriodo: any;
  load: boolean;

  constructor(private authService: AuthService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.load=false;
    this.authService.getAutoFull()
        .then( data => {
          this.automatizados = data;
        });

    this.authService.getServicios()
        .then( data => {
          this.servicios = data;
        });

  }


  crearReporte(){


    this.load=true;
    if(this.fechaInicio && this.fechaFinal && this.tipo){

      this.authService.getServiciosPeriodo(this.datePipe.transform(this.fechaInicio, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy'), this.tipo)
          .then( data => {
            this.servicioPeriodo = data;
              this.load=false;
          });
    }else{
      alert('Ingrese los datos solicitados');
    }
  }
}
