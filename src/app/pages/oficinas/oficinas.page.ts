import { Component, OnInit } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import {AuthService} from '../../services/auth.service';
@Component({
  selector: 'app-oficinas',
  templateUrl: './oficinas.page.html',
  styleUrls: ['./oficinas.page.scss'],
})
export class OficinasPage implements OnInit {


  slideOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 1,
    autoplay: true
  };

  oficinas: any;
  constructor(private authService: AuthService) { }

  ngOnInit() {

    this.authService.getOficinas()
        .then( data => {
          this.oficinas = data;
        });
  }

  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }
}
