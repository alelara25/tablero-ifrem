import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OficinasPage } from './oficinas.page';

describe('OficinasPage', () => {
  let component: OficinasPage;
  let fixture: ComponentFixture<OficinasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OficinasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OficinasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
