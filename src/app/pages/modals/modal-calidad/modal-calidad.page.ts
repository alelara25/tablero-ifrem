import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-calidad',
  templateUrl: './modal-calidad.page.html',
  styleUrls: ['./modal-calidad.page.scss'],
})
export class ModalCalidadPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismissModal() {
    this.modalCtrl.dismiss();
  }

}
