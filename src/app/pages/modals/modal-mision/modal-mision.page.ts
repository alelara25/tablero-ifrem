import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-mision',
  templateUrl: './modal-mision.page.html',
  styleUrls: ['./modal-mision.page.scss'],
})
export class ModalMisionPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }
  dismissModal() {
    this.modalCtrl.dismiss();
  }

}
