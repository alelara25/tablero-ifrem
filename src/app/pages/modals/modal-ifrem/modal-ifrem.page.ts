import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-ifrem',
  templateUrl: './modal-ifrem.page.html',
  styleUrls: ['./modal-ifrem.page.scss'],
})
export class ModalIfremPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }
  dismissModal() {
    this.modalCtrl.dismiss();
  }
}
