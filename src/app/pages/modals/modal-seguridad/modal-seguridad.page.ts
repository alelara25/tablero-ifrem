import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-seguridad',
  templateUrl: './modal-seguridad.page.html',
  styleUrls: ['./modal-seguridad.page.scss'],
})
export class ModalSeguridadPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismissModal() {
    this.modalCtrl.dismiss();
  }
}
