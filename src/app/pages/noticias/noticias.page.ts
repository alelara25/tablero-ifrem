import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.page.html',
  styleUrls: ['./noticias.page.scss'],
})
export class NoticiasPage implements OnInit {

  noticias: any;
  constructor(private authService: AuthService) { }

  slideOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 1,
    autoplay: true
  };

  ngOnInit() {

    this.authService.getNoticias()
        .then( data => {
          this.noticias = data;
        });
  }

  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }

}
