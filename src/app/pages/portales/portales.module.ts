import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PortalesPageRoutingModule } from './portales-routing.module';

import { PortalesPage } from './portales.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PortalesPageRoutingModule
  ],
  declarations: [PortalesPage]
})
export class PortalesPageModule {}
