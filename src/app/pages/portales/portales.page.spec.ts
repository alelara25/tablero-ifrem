import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PortalesPage } from './portales.page';

describe('PortalesPage', () => {
  let component: PortalesPage;
  let fixture: ComponentFixture<PortalesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PortalesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
