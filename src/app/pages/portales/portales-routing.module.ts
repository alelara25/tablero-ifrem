import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortalesPage } from './portales.page';

const routes: Routes = [
  {
    path: '',
    component: PortalesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PortalesPageRoutingModule {}
