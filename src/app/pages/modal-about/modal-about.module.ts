import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalAboutPageRoutingModule } from './modal-about-routing.module';

import { ModalAboutPage } from './modal-about.page';
import { ModalIfremPage } from '../modals/modal-ifrem/modal-ifrem.page';
import { ModalIfremPageModule } from '../modals/modal-ifrem/modal-ifrem.module';
import { ModalMisionPage } from '../modals/modal-mision/modal-mision.page';
import { ModalSeguridadPage } from '../modals/modal-seguridad/modal-seguridad.page';
import { ModalCalidadPageModule } from '../modals/modal-calidad/modal-calidad.module';
import { ModalSeguridadPageModule } from '../modals/modal-seguridad/modal-seguridad.module';
import { ModalCalidadPage } from '../modals/modal-calidad/modal-calidad.page';
import { ModalMisionPageModule } from '../modals/modal-mision/modal-mision.module';

@NgModule({
  entryComponents: [
    ModalIfremPage,
    ModalMisionPage,
    ModalSeguridadPage,
    ModalCalidadPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalAboutPageRoutingModule,
    ModalIfremPageModule,
    ModalCalidadPageModule,
    ModalSeguridadPageModule,
    ModalMisionPageModule
  ],
  declarations: [ModalAboutPage]
})
export class ModalAboutPageModule {}
