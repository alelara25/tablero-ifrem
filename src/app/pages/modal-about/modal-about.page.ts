import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalCalidadPage } from '../modals/modal-calidad/modal-calidad.page';
import { ModalIfremPage } from '../modals/modal-ifrem/modal-ifrem.page';
import { ModalMisionPage } from '../modals/modal-mision/modal-mision.page';
import { ModalSeguridadPage } from '../modals/modal-seguridad/modal-seguridad.page';


@Component({
  selector: 'app-modal-about',
  templateUrl: './modal-about.page.html',
  styleUrls: ['./modal-about.page.scss'],
})
export class ModalAboutPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async abrirIfrem() {
   const modal = await this.modalCtrl.create({
    component: ModalIfremPage
   });

   await modal.present();
  }

  async abrirMision() {
    const modal = await this.modalCtrl.create({
     component: ModalMisionPage
    });
    await modal.present();
   }

   async abrirCalidad() {
    const modal = await this.modalCtrl.create({
     component: ModalCalidadPage
    });
    await modal.present();
   }

   async abrirSeguridad() {
    const modal = await this.modalCtrl.create({
     component: ModalSeguridadPage
    });
    await modal.present();
   }


}
