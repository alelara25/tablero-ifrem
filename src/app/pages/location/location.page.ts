import { Component, OnInit } from '@angular/core';
import { Geolocation, Geoposition} from '@ionic-native/geolocation/ngx';
import { Platform } from '@ionic/angular';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
declare var google: any;


@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {
  map = null;

  waypoints: any[];
  positionActual = null;
  informacionLoc: string;
  coordenadasOriLt: any;
  coordenadasOriLn: any;

  // Location Dates
  titleUbicationDestination: string;
  directionDestination: string;
  distanceDestination: string;
  timeTravelDestination: string;

  // map: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  distanceService = new google.maps.DistanceMatrixService();
  bounds = new google.maps.LatLngBounds();

  icon = {
    url: 'assets/Ubi.png', // image url
    scaledSize: new google.maps.Size(30, 50), // scaled size
  };

  markers: any[] = [
    {
      latitude: 19.315049,
      longitude: -99.636455,
      title: 'Toluca'
    },
    {
      latitude: 19.568962,
      longitude: -99.763869,
      title: 'Ixtlahuaca'
    },
    {
      latitude: 19.204313,
      longitude: -100.127775,
      title: 'Valle de Bravo'
    },
    {
      latitude: 18.965647,
      longitude: -99.586022,
      title: 'Tenancingo'
    },
    {
      latitude: 19.256688,
      longitude: -98.891404,
      title: 'Chalco'
    },
    {
      latitude: 19.802352,
      longitude: -100.131289,
      title: 'El Oro'
    },
    {
      latitude: 9.549691,
      longitude: -99.193409,
      title: 'Tlalnepantla'
    },
    {
      latitude: 19.479927,
      longitude: -99.234951,
      title: 'Naucalpan'
    },
    {
      latitude: 19.656881,
      longitude: -99.170158,
      title: 'Cuautitlan'
    },
    {
      latitude: 19.389393,
      longitude: -99.024097,
      title: 'Nezahualcoyotl'
    },
    {
      latitude: 19.043201,
      longitude: -100.042382,
      title: 'Temascaltepec'
    },
    {
      latitude: 19.965249,
      longitude: -99.530339,
      title: 'Jilotepec'
    },
    {
      latitude: 18.859112,
      longitude: -99.966711,
      title: 'Sultepec'
    },
    {
      latitude: 19.284211,
      longitude: -99.517896,
      title: 'Lerma'
    },
    {
      latitude: 19.548106,
      longitude: -99.051685,
      title: 'Ecatepec'
    },
    {
      latitude: 19.800942,
      longitude: -99.103717,
      title: 'Zumpango'
    },
    {
      latitude: 19.701758,
      longitude: -98.759404,
      title: 'Otumba'
    },
    {
      latitude: 19.505574,
      longitude: -98.883338,
      title: 'Texcoco'
    },
    {
      latitude: 19.102353,
      longitude: -99.588877,
      title: 'Tenango'
    },
  ];


  origin = {lat: 19.315066, lng: -99.636208};
  destination = {lat: 19.258493, lng: -99.633945};

  constructor( private geolocation: Geolocation, public platform: Platform, public locationAccuracy: LocationAccuracy) {
    console.log('entro  funcion');
    /*platform.ready().then(() => {
      this.loadMap();
    });*/
  }

/*ionViewDidLoad() {
    this.loadMap();
    console.log('map called');
  }*/

ngOnInit() {
  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    () => {
     this.loadMap();
    },
    error => alert('Error requesting location permissions ' + JSON.stringify(error))
  );
}

loadMap() {
  console.log('entro  funcion');
  const mapEle: HTMLElement = document.getElementById('map');

  this.map = new google.maps.Map(mapEle, {
    center: this.origin,
    zoom: 8
  });

  this.directionsDisplay.setMap(this.map);

  google.maps.event.addListenerOnce(this.map, 'idle', () => {
   mapEle.classList.add('show-map');
  });

  this.markers.forEach(marker => {
    console.log(marker);
    this.addMarker(marker);
  });

}

addMarker(markers) {
  const position = new google.maps.LatLng(markers.latitude, markers.longitude);
  const officeMarker = new google.maps.Marker({
    position,
    title: markers.title,
    icon: this.icon
  });
  this.addInfoWindow(officeMarker, markers.title);
  officeMarker.setMap(this.map);
}

addInfoWindow(marker, title) {
  const infoWindow = new google.maps.InfoWindow({
      content: '<div style="color:rgb(73, 73, 243);"> <center> ' +
      ' <font size=2><b>Oficina Registral de<br>' + title + ' </b> </font></center></div>'
  });

  google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
  });
}


location() {
  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    () => {
  // this.informacionLoc = null;
  console.log('entro');
  this.geolocation.getCurrentPosition().then((geoposition: Geoposition) => {
    this.coordenadasOriLt = geoposition.coords.latitude;
    this.coordenadasOriLn = geoposition.coords.longitude;
   // alert('lat' + geoposition.coords.latitude + '- long' + geoposition.coords.longitude);
  }).catch((error) => {
    console.log('Error getting location', error);
  }).finally(() => {
    console.log('Esto trae::: antes: ' + this.informacionLoc);
    this.calculateDistance(this.coordenadasOriLt, this.coordenadasOriLn);
    console.log('Esto trae::: despues: ' + this.informacionLoc);
    const dt = this.informacionLoc.split('|');
    console.log('Coordenadas' + dt[4] + ' - ' + dt[5]);
    this.calculateRoute(this.coordenadasOriLt, this.coordenadasOriLn, parseFloat(dt[4]), parseFloat(dt[5]));
  });
  },
  error => alert('Error requesting location permissions ' + JSON.stringify(error))
 );
}

private calculateRoute(x, y, a, b) {
  console.log('Dts Coordenadas: ' + x + ' ' + y + ' ' + a + ' ' + b);
  this.directionsService.route({
   origin: {lat: x, lng: y},
   destination: {lat: a, lng: b},
   travelMode: google.maps.TravelMode.DRIVING,
  }, (response, status) => {
    if (status === google.maps.DirectionsStatus.OK) {
     this.directionsDisplay.setDirections(response);
    } else {
      alert('Could not display directions due to: ' + status);
    }
  });
 }

calculateDistance(lt, ln) {
  console.log('datesDistance:: ' + lt + ' ' + ln);
  // tslint:disable-next-line:variable-name
  let _distanceA = 0;
  // tslint:disable-next-line:variable-name
  let _distanceB = 0;
  // tslint:disable-next-line:variable-name
  let _infoLocation = '';
  let aux;
  this.markers.forEach(marker => {
      this.distanceService.getDistanceMatrix({
        origins: [{lat: lt, lng: ln}],
        destinations: [{lat: marker.latitude, lng: marker.longitude}],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC.km,
        durationInTraffic: true,
        avoidHighways: false,
        avoidTolls: false
    }, (respons, statu) => {
      console.log('Response:: ' + statu);
      if (statu === 'OK') {
        const origins = respons.originAddresses;
        console.log(origins);
        const destinations = respons.destinationAddresses;
        console.log(destinations);
        // tslint:disable-next-line:no-var-keyword
        for (let i = 0; i < origins.length; i++) {
          const results = respons.rows[i].elements;
          for (let j = 0; j < results.length; j++) {
            const element = results[j];
            const distance = element.distance.text;
            console.log('Distancia: ' + distance);
            _distanceA = distance.replace(' km', '').replace(' m', '');
            console.log('Replace: ' + _distanceA);
            if (distance.indexOf(' m') >= 0) {
              aux = '0.' + _distanceA;
              _distanceA = aux;
              console.log('AUX: ' + _distanceA);
            }
            console.log('DistanceA: ' + Math.round(_distanceA) + ' DistanceB: ' + Math.round(_distanceB)
             + ' IndexOf: ' + distance.indexOf(' m'));
            if ((Math.round(_distanceA) < Math.round(_distanceB)) || (_distanceB === 0)) {
              console.log ('DistanceB Antes: ' + _distanceB);
              _distanceB = _distanceA;
              console.log ('DistanceB: ' + _distanceB);
              const duration = element.duration.text;
              console.log('Duracion: ' + duration);
              const from = origins[i];
              const to = destinations[j];
              _infoLocation = destinations[j] + '|'
              + origins[i] + '|'
              + distance + '|'
              + marker.title + '|'
              + marker.latitude + '|'
              + marker.longitude;
              this.titleUbicationDestination = 'OFICINA REGISTRAL: ' + marker.title;
              this.directionDestination = 'DIRECCIÓN: ' + destinations[j];
              this.distanceDestination = 'DISTANCIA: ' + distance;
              this.timeTravelDestination = 'TIEMPO EN LLEGAR: ' + duration;
              console.log('Paso IF: ' + _infoLocation);
              this.informacionLoc = _infoLocation;
            } else { console.log('NO ENTRO'); }
          }
        }
      }
    });
  });
  console.log('Dats. ' + this.informacionLoc);
  // return _infoLocation;
 }
}
