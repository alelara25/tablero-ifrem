import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MensualesPageRoutingModule } from './mensuales-routing.module';

import { MensualesPage } from './mensuales.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MensualesPageRoutingModule
  ],
  declarations: [MensualesPage]
})
export class MensualesPageModule {}
