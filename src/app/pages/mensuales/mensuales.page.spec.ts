import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MensualesPage } from './mensuales.page';

describe('MensualesPage', () => {
  let component: MensualesPage;
  let fixture: ComponentFixture<MensualesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensualesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MensualesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
