import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MensualesPage } from './mensuales.page';

const routes: Routes = [
  {
    path: '',
    component: MensualesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MensualesPageRoutingModule {}
