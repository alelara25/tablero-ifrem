import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Chart } from 'chart.js';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-mensuales',
  templateUrl: './mensuales.page.html',
  styleUrls: ['./mensuales.page.scss'],
})
export class MensualesPage implements OnInit {

  tramitesOficina: any;
  tramitesOficinaDesc: any;
  tramitesNotarios: any;
  fechaInicio: any;
  fechaFinal: any;
  etiquetas: any[] = [];
  valores: string[] = [];
  tramitesTotal: any;
  load: boolean;

  stringArr: string[] = [];

  lineChart: any;
  @ViewChild("lineCanvas", { read: ElementRef, static: true }) private lineCanvas: ElementRef;

  doughnutChart: any;
  @ViewChild("doughnutCanvas", { read: ElementRef, static: true }) private doughnutCanvas: ElementRef;

  doughnutChartNot: any;
  @ViewChild("doughnutCanvasNot", { read: ElementRef, static: true }) private doughnutCanvasNot: ElementRef;




  multi: any[];
  view: any[] = [750, 300];

  // options
  legend: boolean = false;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Mes';
  yAxisLabel: string = 'Cantidad';
  timeline: boolean = true;

  label: string = '';

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor(private authService: AuthService, private datePipe: DatePipe) {  }

  ngOnInit() {
    //this.yourCustomFunctionName();
    this.load=false;
  }

  crearReporte(){
    console.log('fechaInicio: ' + this.fechaInicio);
    console.log('fechaFinal: ' + this.fechaFinal);

    this.load=true;
    if(this.fechaInicio && this.fechaFinal){

       this.authService.getTramitesOficina(this.datePipe.transform(this.fechaInicio, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy'), '1')
        .then( data => {
          this.tramitesOficina = data;
        });

        this.authService.getTramitesOficina(this.datePipe.transform(this.fechaInicio, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy'), '2')
        .then( data => {
          this.tramitesOficinaDesc = data;
        });

        this.authService.getTramitesOficina(this.datePipe.transform(this.fechaInicio, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy'), '3')
        .then( data => {
          this.tramitesNotarios = data;
        });

        this.authService.getTramitesTotal(this.datePipe.transform(this.fechaInicio, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy'))
        .then( data => {
          this.tramitesTotal = data;
        });

    this.yourCustomFunctionName();

      this.load=false;

  }else{
    alert('Ingrese los datos solicitados');
  }
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  public yourCustomFunctionName() {

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        //labels: [ this.label , "Orange"],
        labels: [],
        datasets: [
          {
            label: "# of Votes",
           // data: [12, 19, 3, 5, 2, 3],
            data : [],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
              "rgba(75, 192, 192, 0.2)"

            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56","#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56","#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56","#FFC5"]
          }
        ]
      }
    });

    this.authService.getTramitesOficina(this.datePipe.transform(this.fechaInicio, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy'), '1').then(data => {
      // tslint:disable-next-line:no-unused-expression
      new Promise<boolean>((res) => {
        for (let elemento of Object.values(data)) {
          this.doughnutChart.data.datasets[0].data.push(elemento.cantidad);
          this.doughnutChart.data.labels.push(elemento.oficina);
          this.doughnutChart.update();
        }
        res(true);
      });
    });




    // notarios

    this.doughnutChartNot = new Chart(this.doughnutCanvasNot.nativeElement, {
      type: "doughnut",
      data: {
        //labels: [ this.label , "Orange"],
        labels: [],
        datasets: [
          {
            label: "# of Votes",
           // data: [12, 19, 3, 5, 2, 3],
            data : [],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)",
              "rgba(75, 192, 192, 0.2)"

            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56","#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56","#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56","#FFC5"]
          }
        ]
      }
    });

    this.authService.getTramitesDetalles(this.datePipe.transform(this.fechaInicio, 'dd/MM/yyyy'), this.datePipe.transform(this.fechaFinal, 'dd/MM/yyyy'), '3').then(data => {
      // tslint:disable-next-line:no-unused-expression
      new Promise<boolean>((res) => {
        for (let elemento of Object.values(data)) {
          this.doughnutChartNot.data.datasets[0].data.push(elemento.cantidad);
          this.doughnutChartNot.data.labels.push(elemento.oficina);
          this.doughnutChartNot.update();
        }
        res(true);
      });
    });


    console.log("DATA: ", this.doughnutChart.data);
    console.log("DATA: ", this.doughnutChart.data.labels);
    console.log('Esto vale chart.js B:', this.doughnutChart);
  }

}
