import { Component, OnInit } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { AlertService } from './services/alert.service';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import {Ventana} from './models/user';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;

  showSplash = true;
  splash = true;
  logginIn = false;
  status: Ventana;

  /*public appPages = [
    {
      title: 'Inbox',
      url: '/folder/Inbox',
      icon: 'mail'
    },
    {
      title: 'Outbox',
      url: '/folder/Outbox',
      icon: 'paper-plane'
    },
    {
      title: 'Favorites',
      url: '/folder/Favorites',
      icon: 'heart'
    },
    {
      title: 'Archived',
      url: '/folder/Archived',
      icon: 'archive'
    },
    {
      title: 'Trash',
      url: '/folder/Trash',
      icon: 'trash'
    },
    {
      title: 'Spam',
      url: '/folder/Spam',
      icon: 'warning'
    }
  ];*/
  public appPagesLogin = [
    {
      title: 'Logín',
      url: '/login',
      icon: 'person-outline'
    }

  ];
  public appPagesFree = [

    {
      title: 'Tramites',
      url: '/tramites',
      icon: 'document-text-outline'
    },
    {
      title: 'Oficinas Registrales',
      url: '/oficinas',
      icon: 'business-outline'
    },
    {
      title: 'Oficina Más Cercana',
      url: '/location',
      icon: 'map'
    },
    {
      title: 'Portales',
      url: '/portales',
      icon: 'earth-outline'
    },
    {
      title: 'Noticias',
      url: '/noticias',
      icon: 'megaphone-outline'
    },
    {
      title: 'Contacto',
      url: '/contacto',
      icon: 'mail-outline'
    },
    {
     title: 'Quejas',
      url: '/quejas',
      icon: 'chatbox-ellipses-outline'
    },
    {
      title: 'Acerca de',
      url: '/about',
      icon: 'information-circle-outline'
    },
    {
      title: 'Inicio',
      url: '/login',
      icon: 'home-outline'
    }
  ];

  public appPages = [
    {
      title: 'Dashboard',
      url: '/dashboard',
      icon: 'pulse-outline'
    },
    {
      title: 'Trámites',
      url: '/mensuales',
      icon: 'calendar'
    },
    {
      title: 'Gráfica Anual',
      url: '/anual',
      icon: 'bar-chart-outline'
    },
    {
      title: 'Gráfica por Fechas',
      url: '/dinamic-module',
      icon: 'podium-outline'
    },
    {
      title: 'Finanzas',
      url: '/finanzas',
      icon: 'cash-outline'
    },
    {
      title: 'Automatizados',
      url: '/automatizados',
      icon: 'cloud-upload-outline'
    }

  ];


  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      //this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(2000).subscribe(() => this.showSplash = false); // <-- hide animation after 3s
      setTimeout(() => this.splash = false, 5500);


      this.authService.getEstado().subscribe((res) => {
        console.log('check-usada: ' , res);
        this.status = res;
      });

      this.authenticationService.authState.subscribe(state => {
        if (state) {
          this.logginIn=true;
          this.router.navigate(['about']);
        } else {
          this.router.navigate(['login']);
        }
      });

    });
  }

  logoutUser(){
    this.logginIn = false;
    this.authenticationService.logout();
  }



  /*initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }*/
}
