import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})

export class AnualService {

  constructor(
    private http: HttpClient,
    private env: EnvService) { }

  getData(params: string) {
    return new Promise(resolve => {
      this.http.get(this.env.API_URL + 'getData' + params).subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
      });
    });
  }
}
