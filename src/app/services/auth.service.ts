import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './env.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import {User, Ventana} from '../models/user';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {



  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService) { }




    getFres(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'fres').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getImagenes(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'imagenes').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getFechas(fechaInicial: string, fechaFinal: string) {
      return new Promise(resolve => {
        this.http.get(this.env.API_URL + '/getData?fechaInicial=' + fechaInicial + '&fechaFinal=' + fechaFinal).subscribe(data => {
          resolve(data);
         }, error => {
        console.log(error);
      });
      });
    }


    getTramitesCorrientes(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesIngresadosCorriente').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesAuto(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesAuto').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesLinea(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesLinea').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesOrdinario(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesOrdinario').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesClg(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesClg').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesFirmados(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesFirmados').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesAceptados(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesAceptados').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesSusp(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesSusp').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesRechazo(){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'tramitesRechazo').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesOficina(fechaInicio: string, fechaFin: string, tipo: string){
      console.log('dates: ' + fechaInicio + ' - ' + fechaFin);
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'getTramitesOficina?fechaInicial=' + fechaInicio + '&fechaFinal=' + fechaFin + '&tipo=' + tipo).subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }


    getTramitesDetalles(fechaInicio: string, fechaFin: string, tipo: string){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'getTramitesTotalDetalle?fechaInicial=' + fechaInicio + '&fechaFinal=' + fechaFin ).subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getTramitesTotal(fechaInicio: string, fechaFin: string){
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL+'getTramitesTotal?fechaInicial=' + fechaInicio + '&fechaFinal=' + fechaFin ).subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getDinero(fechaInicio: string, fechaFin: string){
      return new Promise(resolve=>{
        console.log('salida: ', this.env.API_URL+'getRecaudacion?fechaInicial='+fechaInicio+'&fechaFinal='+fechaFin);
        this.http.get(this.env.API_URL+'getRecaudacion?fechaInicial='+fechaInicio+'&fechaFinal='+fechaFin).subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getFinanzas(tipo: string){
      console.log('INIT-finanzas: ' + tipo);
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL_PAGOS+'getServicioCantidad?tipo=' + tipo).subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

    getBancos(){
      console.log('INIT-bancos: ' );
      return new Promise(resolve=>{
        this.http.get(this.env.API_URL_PAGOS+'getPagosBancos').subscribe(data=>{
            resolve(data);
        },error=>{
          console.log(error);
        });
      });
    }

  getAutoFull(){
    console.log('INIT-getAutoFull: ' );
    return new Promise(resolve=>{
      this.http.get(this.env.API_URL+'getAutomatizados').subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }

  getServicios(){
    console.log('INIT-getAutoFull: ' );
    return new Promise(resolve=>{
      this.http.get(this.env.API_URL+'getActosAuto').subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }

  getServiciosPeriodo(fechaInicio: string, fechaFin: string, tipo: string){
    console.log('INIT-getAutoFull: ' );
    return new Promise(resolve=>{
      this.http.get(this.env.API_URL+"getAutomatizadosFechas?fechaInicial="+fechaInicio+"&fechaFinal="+fechaFin+"&tipo="+tipo).subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }

  getNoticias(){
    return new Promise(resolve=>{
      this.http.get('https://msconsulta.ifrem.gob.mx/getNoticias').subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }

  getOficinas(){
    return new Promise(resolve=>{
      this.http.get('https://msconsulta.ifrem.gob.mx/getOficinas').subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }


  getTramite(tramite: string, oficina: string) {
    return new Promise(resolve=>{
      this.http.get(`${this.env.API_URL_GRAL}/getTramiteOficina?numero=${tramite}&oficina=${oficina}`).subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }


  getFormatos() {
    return new Promise(resolve=>{
      this.http.get(`${this.env.API_URL_GRAL}/getFormatos`).subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }


  getLogin(){
    return new Promise(resolve=>{
      this.http.get(this.env.API_URL+'loginStatus').subscribe(data=>{
        resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }

  public getEstado() {
    return this.http.get<Ventana>(this.env.API_URL + 'loginStatus');
  }

  postQueja(datos){ // metodo que vamos ocupar

    var url= "https://msconsulta.ifrem.gob.mx/api/v1/servicios/quejas";
    return this.http.post(url,datos,
      {headers:new HttpHeaders (
        {"content-Type":"application/json"})});
      }
  // return new Promise(resolve=>{ // promesa que esprea el resultado
  // aqui ocupamos la raiz de ms.go
  //    this.http.get(this.env.API_URL+'quejas').subscribe(data=>{ // aqui va el metodo a ocupar
  //      resolve(data);
  //    },error=>{
  //      console.log(error);
  //    });
  //  });
  //}

}
