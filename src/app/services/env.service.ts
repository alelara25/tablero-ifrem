import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  
  API_URL = 'https://msconsulta.ifrem.gob.mx/api/v1/servicios/';
  API_URL_PAGOS = 'https://mspagos.ifrem.gob.mx/';
  API_URL_GRAL = 'https://msconsulta.ifrem.gob.mx/';


  //API_URL = 'http://localhost:8080/api/v1/servicios/';
  //API_URL_PAGOS = 'https://mspagos.ifrem.gob.mx/';
  //API_URL_GRAL = 'http://localhost:8080/';
  constructor() { }
}
