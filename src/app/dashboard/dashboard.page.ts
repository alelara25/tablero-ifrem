import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  totalFres: any;
  totalTramite: any;
  totalAuto: any;
  totalLinea: any;
  totalOrdinario: any;
  totalClg: any;
  totalImagenes: any;
  totalFirma: any;
  totalAcptado: any;
  totalSusp: any;
  totalRechazo: any;
  

  constructor(
    private authServiceLog: AuthenticationService,
    private authService: AuthService
    ) { }

  ngOnInit() {
    console.log('iniciandoINIT()');

    this.authService.getFres().then(data => {
      this.totalFres= data;
      console.log ('TOTAL DE FRES: ' + this.totalFres);
    });

    this.authService.getImagenes().then(data => {
      this.totalImagenes= data;
      console.log ('TOTAL DE totalImagenes: ' + this.totalImagenes);
    });

    this.authService.getTramitesCorrientes().then(data => {
      this.totalTramite= data;
      console.log ('TOTAL DE FRES: ' + this.totalTramite);
    });

    this.authService.getTramitesAuto().then(data => {
      this.totalAuto= data;
      console.log ('TOTAL DE FRES: ' + this.totalAuto);
    });

    this.authService.getTramitesLinea().then(data => {
      this.totalLinea= data;
      console.log ('TOTAL DE FRES: ' + this.totalLinea);
    });

    this.authService.getTramitesOrdinario().then(data => {
      this.totalOrdinario= data;
      console.log ('TOTAL DE FRES: ' + this.totalOrdinario);
    });

    this.authService.getTramitesClg().then(data => {
      this.totalClg= data;
      console.log ('TOTAL DE FRES: ' + this.totalClg);
    });



    this.authService.getTramitesFirmados().then(data => {
      this.totalFirma= data;
      console.log ('TOTAL DE FRES: ' + this.totalFirma);
    });

    this.authService.getTramitesAceptados().then(data => {
      this.totalAcptado= data;
      console.log ('TOTAL DE FRES: ' + this.totalAcptado);
    });

    this.authService.getTramitesSusp().then(data => {
      this.totalSusp= data;
      console.log ('TOTAL DE FRES: ' + this.totalSusp);
    });

    this.authService.getTramitesRechazo().then(data => {
      this.totalRechazo= data;
      console.log ('TOTAL DE FRES: ' + this.totalRechazo);
    });
  }

  logoutUser(){
    this.authServiceLog.logout();
   }
  }
